LibertyCode_AttributeModel
==========================



Description
-----------

Library contains attribute model components, 
to handle model components.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Framework library implementation requirement
-----------------------------------------

1. Library repository: liberty_code/validation: version 1.0

    - Standard rules implementation (or equivalent): 
        
        Validator provided on entities, 
        must contain all standard rules, 
        added on its rule collection.
        
    - Validator rules implementation (or equivalent): 
    
        Validator provided on entities, 
        must contain all validator rules, 
        added on its rule collection.
        Each validator rule must use validator,
        with same implementation as validator provided on entities.
        
2. Library repository: liberty_code/sql: version 1.0

    - Validation SQL rules implementation (or equivalent): 
        
        Validator provided on entities, 
        must contain all SQL rules, 
        added on its rule collection.
        
---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code/attribute_model ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code/attribute_model": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Usage
-----

#### Attribute

Attribute entity allows to design attribute, 
using entity.

_Elements_

- AttributeEntity

    Extends save configured entity features. 
    Uses entity to design an attribute,
    can be used on entity.

- AttributeEntityCollection

    Extends fixed entity collection features. 
    Allows to design collection of attribute entities.
    Allows to provide entity attributes configuration.

- AttributeEntityFactory

    Extends fixed entity factory features. 
    Allows to design attribute factory, 
    to provide attribute entity objects,
    from specified configuration.
     
- AttributeEntityRepository

    Extends fixed multi repository features.
    Allows to design attribute entity repository,
    to save data, from attribute entity, in persistence.

- AttributeEntityCollectionRepository

    Extends fixed multi collection repository features. 
    Allows to design attribute entity collection repository,
    to save data, from attribute entity collection, in persistence.

- SqlAttributeEntity

    Extends attribute entity features. 
    Uses SQL rules for attributes validation.

- SqlAttributeEntityFactory

    Extends attribute entity factory features. 
    Provides SQL attribute entity objects.
    
- SqlAttributeEntityRepository

    Extends attribute entity repository features.
    Uses SQL table persistor, as persistence. 
    
- SqlAttributeEntityCollectionRepository

    Extends attribute entity collection repository features. 
    Uses SQL table persistor, as persistence. 
    
- SaveAttributeEntity

    Extends attribute entity features. 
    Can be used on save entity.
    
- SaveAttributeEntityCollection

    Extends attribute entity collection features. 
    Allows to provide save entity attributes configuration.

- SaveAttributeEntityFactory

    Extends attribute entity factory features. 
    Provides save attribute entity objects.
    
- SqlSaveAttributeEntity

    Extends save attribute entity features. 
    Uses SQL rules for attributes validation.

- SqlSaveAttributeEntityFactory

    Extends save attribute entity factory features. 
    Provides SQL save attribute entity objects.
    
```php
// Get attribute entity factory
use liberty_code\attribute_model\attribute\model\AttributeEntityFactory;
$attributeEntityFactory = new AttributeEntityFactory($provider);
...
// Get attribute builder
use liberty_code\handle_model\attribute\build\model\DefaultBuilder;
$attributeBuilder = new DefaultBuilder($attributeFactory);
...
// Get attribute entity collection
use liberty_code\attribute_model\attribute\model\AttributeEntityCollection;
$attributeEntityCollection = new AttributeEntityCollection();
...
// Hydrate attribute entity collection
$attributeBuilder->setTabDataSrc(array(...));
$attributeBuilder->hydrateAttributeCollection($attributeEntityCollection);
...
foreach($attributeEntityCollection->getTabAttributeKey() as $attributeKey) {
    echo($attributeEntityCollection->getObjAttribute($attributeKey)->getStrAttributeKey() .'<br />');
}
/**
 * Show: 
 * Attribute key 1
 * ...
 * Attribute key N
 */
...
```

#### Attribute browser

Browser allows to provide attribute entities, 
from specified browsing configuration.

- AttributeBrowserEntity

    Extends validator configured entity features.
    Allows to define attributes,
    to search attribute entities.

- AttributeBrowserRepository

    Allows to load attribute entities,
    from specified attribute browser entity
    or specified browsing configuration.

- SqlAttributeBrowserRepository

    Extends attribute browser repository features. 
    Uses SQL table browser, 
    to load attribute entities. 

```php
// Get attribute browser entity
use liberty_code\attribute_model\attribute\browser\model\AttributeBrowserEntity;
$attributeBrowserEntity = new AttributeBrowserEntity(
    array(), 
    $validator,
    $dateTimeFactory
    $attrSpec
);
...
// Hydrate attribute browser entity
$roleBrowserEntity->intAttrItemCountPage = 50; // Select 50 roles per page
$roleBrowserEntity->intAttrPageIndex = 0; // Select Page 1
$roleBrowserEntity->strAttrCritLikeName = '...' // Select result where attribute name like '...'
$roleBrowserEntity->$strAttrSortName = 'asc' // Sort result by ascending attribute name
...
// Get attribute browser repository
use liberty_code\attribute_model\attribute\browser\sql\model\repository\SqlAttributeBrowserRepository;
$attributeBrowserRepository = new SqlAttributeBrowserRepository(
    $sqlTableBrowser, 
    $sqlAttributeEntityCollectionRepository
);
...
// Load attribute collection
$attributeBrowserRepository->loadFromEntity($attributeEntityCollection, $attributeBrowserEntity);
...
```

#### Attribute provider

Repository attribute provider allows to provide attribute information for entity, 
using attribute entity features.

_Elements_

- RepoAttrProvider
    
    Extends standard attribute provider features.
    Uses attribute entity collection, 
    and attribute entity collection repository,
    to provide attribute information for entity.
    
- SqlRepoAttrProvider

    Extends repository attribute provider features. 
    Uses SQL attribute entity collection repository, 
    to provide attribute information for entity. 
    
_Example_

```php
// Get attribute provider
use liberty_code\attribute_model\provider\sql\model\SqlRepoAttrProvider;
$attrProvider = new AttrProvider(
    $attributeEntityCollection, 
    $sqlAttributeEntityCollectionRepository
);
...
// Get entity attribute configuration array
var_dump($attrProvider->getTabEntityAttrConfig());
...
// Get entity attribute rule configurations array
var_dump($attrProvider->getTabEntityAttrRuleConfig());
...
```

---


