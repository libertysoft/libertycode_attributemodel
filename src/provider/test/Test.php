<?php

// Init var
$strTestRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strTestRootAppPath . '/src/attribute/test/AttributeRepositoryTest.php');
require_once($strTestRootAppPath . '/src/provider/test/TestAttrProvider.php');

$strGetClean = (isset($_GET['clean']) ? $_GET['clean'] : '0');
$_GET['clean'] = '0';
$_GET['clean_attribute'] = '0';
ob_start();
require_once($strTestRootAppPath . '/src/attribute/test/Test.php');
ob_get_clean();
$objConnection->connect(); // Reconnect database (previously de-connected)

// Use
use liberty_code\register\register\table\model\DefaultTableRegister;
use liberty_code\cache\repository\model\DefaultRepository;
use liberty_code\attribute_model\attribute\repository\model\SaveAttributeEntityCollection;
use liberty_code\attribute_model\provider\test\TestAttrProvider;



// Init cache repository
$objCacheRepo = new DefaultRepository(
    null,
    new DefaultTableRegister()
);

// Init attribute provider
$objAttributeEntityCollection = new SaveAttributeEntityCollection();
$objAttrProvider = new TestAttrProvider(
    $objAttributeEntityCollection,
    $objAttributeEntityCollectionRepo,
    array(
        'cache_require' => true,
        'cache_entity_attribute_config_key' => 'entity-attribute-config',
        'cache_entity_attribute_rule_config_key' => 'entity-attribute-rule-config',
        'cache_entity_attribute_value_format_get_key_pattern' => 'entity-attribute-%1$s-value-%2$s-format-get',
        'cache_entity_attribute_value_format_set_key_pattern' => 'entity-attribute-%1$s-value-%2$s-format-set',
        'cache_entity_attribute_value_save_format_get_key_pattern' => 'entity-attribute-%1$s-value-%2$s-save-format-get',
        'cache_entity_attribute_value_save_format_set_key_pattern' => 'entity-attribute-%1$s-value-%2$s-save-format-set',
        'cache_attribute_entity_collection_key' => 'attribute-entity-collection'
    ),
    $objCacheRepo
);



// Test get attribute provider
echo('Test get attribute provider: <br />');

echo('Get attribute configuration: <pre>');
var_dump($objAttrProvider->getTabEntityAttrConfig());
echo('</pre>');

echo('Get rule configurations: <pre>');
var_dump($objAttrProvider->getTabEntityAttrRuleConfig());
echo('</pre>');

$tabAttrValue = array(
    $objAttributeEntityCollection[0]->getStrAttributeKey() => ['test', 7], // Ok
    'test' => [], // Ko: Not found
    $objAttributeEntityCollection[1]->getStrAttributeKey() => [7, 'test'], // Ok
    $objAttributeEntityCollection[2]->getStrAttributeKey() => [false, true, 0, 1, 7, 'test'] // Ok
);
foreach($tabAttrValue as $strAttributeKey => $tabValue)
{
    foreach($tabValue as $value)
    {
        echo('Test value, for following attribute key "'.$strAttributeKey.'":');
        var_dump($value);
        echo('<br />');

        echo('Get format get value: <pre>');
        var_dump($objAttrProvider->getEntityAttrValueFormatGet($strAttributeKey, $value));
        echo('</pre>');

        echo('Get format set value: <pre>');
        var_dump($objAttrProvider->getEntityAttrValueFormatSet($strAttributeKey, $value));
        echo('</pre>');

        echo('Get save format get value: <pre>');
        var_dump($objAttrProvider->getEntityAttrValueSaveFormatGet($strAttributeKey, $value));
        echo('</pre>');

        echo('Get save format set value: <pre>');
        var_dump($objAttrProvider->getEntityAttrValueSaveFormatSet($strAttributeKey, $value));
        echo('</pre>');

        echo('<br />');
    }
}

echo('<br /><br /><br />');



// Test remove all attributes
$objAttributeEntityCollection->removeAttributeAll();

echo('Test remove all attributes: <br />');

echo('Get attribute keys: <pre>');
var_dump($objAttributeEntityCollection->getTabAttributeKey());
echo('</pre>');

echo('Get attribute configuration: <pre>');
var_dump($objAttrProvider->getTabEntityAttrConfig());
echo('</pre>');

echo('Get rule configurations: <pre>');
var_dump($objAttrProvider->getTabEntityAttrRuleConfig());
echo('</pre>');

$objCacheRepo->removeItemAll();

echo('Test cache clear: <br />');

echo('Get attribute configuration: <pre>');
var_dump($objAttrProvider->getTabEntityAttrConfig());
echo('</pre>');

echo('Get rule configurations: <pre>');
var_dump($objAttrProvider->getTabEntityAttrRuleConfig());
echo('</pre>');

echo('<br /><br /><br />');



// Remove test database, if required
$_GET['clean'] = $strGetClean;
require($strTestRootAppPath . '/src/attribute/test/db/HelpRemoveDbTest.php');


