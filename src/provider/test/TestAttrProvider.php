<?php

namespace liberty_code\attribute_model\provider\test;

use liberty_code\attribute_model\provider\sql\model\SqlRepoAttrProvider;

use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\attribute_model\attribute\sql\model\repository\SqlAttributeEntityCollectionRepository;
use liberty_code\attribute_model\attribute\repository\model\SaveAttributeEntityCollection;



/**
 * @method SaveAttributeEntityCollection getObjAttributeCollection() @inheritdoc
 * @method void setObjAttributeCollection(SaveAttributeEntityCollection $objAttributeCollection) @inheritdoc
 */
class TestAttrProvider extends SqlRepoAttrProvider
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param SaveAttributeEntityCollection $objAttributeCollection
     * @param SqlAttributeEntityCollectionRepository $objAttributeEntityCollectionRepo
     */
    public function __construct(
        SaveAttributeEntityCollection $objAttributeCollection,
        SqlAttributeEntityCollectionRepository $objAttributeEntityCollectionRepo,
        array $tabConfig = null,
        RepositoryInterface $objCacheRepo = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objAttributeCollection,
            $objAttributeEntityCollectionRepo,
            $tabConfig,
            $objCacheRepo
        );
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixAttributeCollectionClassPath()
    {
        // Return result
        return SaveAttributeEntityCollection::class;
    }



}