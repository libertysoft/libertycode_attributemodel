<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\attribute_model\provider\library;



class ConstRepoAttrProvider
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_REPO_ATTRIBUTE_ENTITY_COLLECTION_REPO = 'objAttributeEntityCollectionRepo';



    // Configuration
    const TAB_CONFIG_KEY_LOAD_ID = 'load_id';
    const TAB_CONFIG_KEY_SEARCH_QUERY = 'search_query';
    const TAB_CONFIG_KEY_REPO_EXECUTION_CONFIG = 'repo_execution_config';
    const TAB_CONFIG_KEY_CACHE_ATTRIBUTE_ENTITY_COLLECTION_KEY = 'cache_attribute_entity_collection_key';

    // Cache configuration
    const CACHE_DEFAULT_ATTRIBUTE_ENTITY_COLLECTION_KEY = 'attribute_entity_collection';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the repository attribute provider configuration standard.';
    const EXCEPT_MSG_ATTRIBUTE_ENTITY_COLLECTION_REPO_INVALID_FORMAT =
        'Following attribute entity collection repository "%1$s" invalid! It must be an attribute entity collection repository object.';



}