<?php
/**
 * Description :
 * This class allows to define repository attribute provider class.
 * Repository attribute provider is standard attribute provider,
 * uses attribute entities, and attribute entity collection repository,
 * to provide attribute information for entity.
 *
 * Repository attribute provider uses the following specified configuration:
 * [
 *     Standard attribute provider configuration,
 *
 *     load_id(optional): [
 *         integer attribute 1 ID,
 *         ...,
 *         integer attribute N ID
 *     ]
 *
 *     search_query(optional): "mixed repository search query",
 *
 *     repo_execution_config(optional): [
 *         @see AttributeEntityCollectionRepository::load() configuration array format
 *         OR
 *         @see AttributeEntityCollectionRepository::search() configuration array format
 *     ]
 *
 *     cache_attribute_entity_collection_key(optional: got @see ConstRepoAttrProvider::CACHE_DEFAULT_ATTRIBUTE_ENTITY_COLLECTION_KEY, if not found):
 *         "string key,
 *         used on cache repository,
 *         to store loaded attribute entity collection",
 * ]
 *
 * Note:
 * - Attribute entity collection loading:
 *     - If load_id provided, uses attribute entity IDs to load attributes.
 *     - If search_query provided, uses query to search attributes.
 *     - Else, uses empty array of attribute entity IDs to load attributes.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\attribute_model\provider\model;

use liberty_code\handle_model\attribute\provider\standard\model\StandardAttrProvider;

use Exception;
use liberty_code\cache\repository\library\ToolBoxRepository;
use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\handle_model\attribute\provider\library\ConstAttrProvider;
use liberty_code\handle_model\attribute\provider\standard\library\ConstStandardAttrProvider;
use liberty_code\attribute_model\attribute\model\AttributeEntityCollection;
use liberty_code\attribute_model\attribute\model\repository\AttributeEntityCollectionRepository;
use liberty_code\attribute_model\provider\library\ConstRepoAttrProvider;
use liberty_code\attribute_model\provider\exception\ConfigInvalidFormatException;
use liberty_code\attribute_model\provider\exception\AttributeEntityCollectionRepoInvalidFormatException;



/**
 * @method AttributeEntityCollectionRepository getObjAttributeEntityCollectionRepo() Get attribute entity collection repository object.
 * @method void setObjAttributeCollection(AttributeEntityCollection $objAttributeCollection) @inheritdoc
 * @method void setObjAttributeEntityCollectionRepo(AttributeEntityCollectionRepository $objAttributeEntityCollectionRepo) Set attribute entity collection repository object.
 */
class RepoAttrProvider extends StandardAttrProvider
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param AttributeEntityCollection $objAttributeCollection
     * @param AttributeEntityCollectionRepository $objAttributeEntityCollectionRepo
     */
    public function __construct(
        AttributeEntityCollection $objAttributeCollection,
        AttributeEntityCollectionRepository $objAttributeEntityCollectionRepo,
        array $tabConfig = null,
        RepositoryInterface $objCacheRepo = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objAttributeCollection,
            $tabConfig,
            $objCacheRepo
        );

        // Init attribute entity collection repository
        $this->setObjAttributeEntityCollectionRepo($objAttributeEntityCollectionRepo);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstRepoAttrProvider::DATA_KEY_REPO_ATTRIBUTE_ENTITY_COLLECTION_REPO))
        {
            $this->__beanTabData[ConstRepoAttrProvider::DATA_KEY_REPO_ATTRIBUTE_ENTITY_COLLECTION_REPO] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstRepoAttrProvider::DATA_KEY_REPO_ATTRIBUTE_ENTITY_COLLECTION_REPO
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstAttrProvider::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                case ConstRepoAttrProvider::DATA_KEY_REPO_ATTRIBUTE_ENTITY_COLLECTION_REPO:
                    $strAttributeEntityCollectionRepoClassPath = $this->getStrFixAttributeEntityCollectionRepoClassPath();
                    AttributeEntityCollectionRepoInvalidFormatException::setCheck(
                        $value,
                        $strAttributeEntityCollectionRepoClassPath
                    );
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixAttributeCollectionClassPath()
    {
        // Return result
        return AttributeEntityCollection::class;
    }



    /**
     * Get fixed attribute entity collection repository class path.
     * Null means no specific class path fixed.
     * Overwrite it to implement specific class path.
     *
     * @return null|string
     */
    protected function getStrFixAttributeEntityCollectionRepoClassPath()
    {
        // Return result
        return null;
    }



    /**
     * Get specified cache key,
     * to store loaded attribute entity collection.
     *
     * @return string
     */
    protected function getStrCacheAttributeEntityCollectionKey()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstRepoAttrProvider::TAB_CONFIG_KEY_CACHE_ATTRIBUTE_ENTITY_COLLECTION_KEY]) ?
                $tabConfig[ConstRepoAttrProvider::TAB_CONFIG_KEY_CACHE_ATTRIBUTE_ENTITY_COLLECTION_KEY] :
                ConstRepoAttrProvider::CACHE_DEFAULT_ATTRIBUTE_ENTITY_COLLECTION_KEY
        );

        // Return result
        return $result;
    }



    /**
     * Get index array of attribute entity IDs,
     * to load attribute entity collection.
     *
     * @return null|array
     */
    protected function getTabLoadId()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstRepoAttrProvider::TAB_CONFIG_KEY_LOAD_ID]) ?
                array_values($tabConfig[ConstRepoAttrProvider::TAB_CONFIG_KEY_LOAD_ID]) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get search query,
     * to load attribute entity collection.
     *
     * @return null|mixed
     */
    protected function getSearchQuery()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstRepoAttrProvider::TAB_CONFIG_KEY_SEARCH_QUERY]) ?
                $tabConfig[ConstRepoAttrProvider::TAB_CONFIG_KEY_SEARCH_QUERY] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get repository execution configuration.
     *
     * @return null|mixed
     */
    protected function getTabRepoExecConfig()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstRepoAttrProvider::TAB_CONFIG_KEY_REPO_EXECUTION_CONFIG]) ?
                $tabConfig[ConstRepoAttrProvider::TAB_CONFIG_KEY_REPO_EXECUTION_CONFIG] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get attribute entity collection engine.
     *
     * @return AttributeEntityCollection
     * @throws ConfigInvalidFormatException
     */
    protected function getObjAttributeEntityCollectionEngine()
    {
        // Init var
        $result = $this->beanGet(ConstStandardAttrProvider::DATA_KEY_DEFAULT_ATTRIBUTE_COLLECTION);
        $objAttributeEntityCollectionRepo = $this->getObjAttributeEntityCollectionRepo();
        $tabLoadId = $this->getTabLoadId();
        $searchQuery = $this->getSearchQuery();
        $tabExecConfig = $this->getTabRepoExecConfig();

        // Load attribute entity collection
        if(
            (!is_null($tabLoadId)) ?
                (!$objAttributeEntityCollectionRepo->load(
                    $result,
                    $tabLoadId,
                    $tabExecConfig
                )) :
                (
                    (!is_null($searchQuery)) ?
                        (!$objAttributeEntityCollectionRepo->search(
                            $result,
                            $searchQuery,
                            $tabExecConfig
                        )) :
                        (!$objAttributeEntityCollectionRepo->load(
                            $result,
                            array(),
                            $tabExecConfig
                        ))
                )

        )
        {
            throw new ConfigInvalidFormatException($this->getTabConfig());
        };

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @return AttributeEntityCollection
     */
    public function getObjAttributeCollection()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            $this->checkCacheRequired() ?
                ToolBoxRepository::getSetItem(
                    $this->getObjCacheRepo(),
                    $this->getStrCacheAttributeEntityCollectionKey(),
                    function() {return $this->getObjAttributeEntityCollectionEngine();},
                    (
                        isset($tabConfig[ConstAttrProvider::TAB_CONFIG_KEY_CACHE_SET_CONFIG]) ?
                            $tabConfig[ConstAttrProvider::TAB_CONFIG_KEY_CACHE_SET_CONFIG] :
                            null
                    )
                ) :
                $this->getObjAttributeEntityCollectionEngine()
        );

        // Return result
        return $result;
    }



}