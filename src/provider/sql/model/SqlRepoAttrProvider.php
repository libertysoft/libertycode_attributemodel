<?php
/**
 * Description :
 * This class allows to define SQL repository attribute provider class.
 * SQL repository attribute provider is repository attribute provider,
 * uses SQL attribute entity collection repository,
 * to provide attribute information for entity.
 *
 * SQL repository attribute provider uses the following specified configuration:
 * [
 *     Repository attribute provider configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\attribute_model\provider\sql\model;

use liberty_code\attribute_model\provider\model\RepoAttrProvider;

use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\attribute_model\attribute\model\AttributeEntityCollection;
use liberty_code\attribute_model\attribute\sql\model\repository\SqlAttributeEntityCollectionRepository;
use liberty_code\attribute_model\provider\library\ConstRepoAttrProvider;



/**
 * @method SqlAttributeEntityCollectionRepository getObjAttributeEntityCollectionRepo() @inheritdoc
 * @method void setObjAttributeEntityCollectionRepo(SqlAttributeEntityCollectionRepository $objAttributeEntityCollectionRepo) @inheritdoc
 */
class SqlRepoAttrProvider extends RepoAttrProvider
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * SQL attribute entity collection repository instance.
     * @var SqlAttributeEntityCollectionRepository
     */
    protected $objAttributeEntityCollectionRepo;
	




	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param SqlAttributeEntityCollectionRepository $objAttributeEntityCollectionRepo
     */
    public function __construct(
        AttributeEntityCollection $objAttributeCollection,
        SqlAttributeEntityCollectionRepository $objAttributeEntityCollectionRepo,
        array $tabConfig = null,
        RepositoryInterface $objCacheRepo = null
    )
    {
        // Init properties
        $this->objAttributeEntityCollectionRepo = $objAttributeEntityCollectionRepo;

        // Call parent constructor
        parent::__construct(
            $objAttributeCollection,
            $objAttributeEntityCollectionRepo,
            $tabConfig,
            $objCacheRepo
        );
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixAttributeEntityCollectionRepoClassPath()
    {
        // Return result
        return SqlAttributeEntityCollectionRepository::class;
    }



    /**
     * Get search query,
     * for fixed configuration array.
     *
     * @return mixed
     */
    protected function getFixConfigSearchQuery()
    {
        // Init var
        $strTableNm = $this
            ->objAttributeEntityCollectionRepo
            ->getObjRepository()
            ->getStrSqlTableName();
        $result = array(
            'select' => [
                ['pattern' => '*']
            ],
            'from' => [
                $strTableNm
            ]
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstRepoAttrProvider::TAB_CONFIG_KEY_LOAD_ID => null,
            ConstRepoAttrProvider::TAB_CONFIG_KEY_SEARCH_QUERY => $this->getFixConfigSearchQuery()
        );
    }



}