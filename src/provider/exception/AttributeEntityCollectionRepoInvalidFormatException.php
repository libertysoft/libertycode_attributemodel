<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\attribute_model\provider\exception;

use Exception;

use liberty_code\attribute_model\attribute\model\repository\AttributeEntityCollectionRepository;
use liberty_code\attribute_model\provider\library\ConstRepoAttrProvider;



class AttributeEntityCollectionRepoInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $repository
     */
	public function __construct($repository)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstRepoAttrProvider::EXCEPT_MSG_ATTRIBUTE_ENTITY_COLLECTION_REPO_INVALID_FORMAT,
            mb_strimwidth(strval($repository), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified repository has valid format.
	 * 
     * @param mixed $repository
     * @param null|string $strFixClassPath = null
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($repository, $strFixClassPath = null)
    {
        // Init var
        $strFixClassPath = (is_string($strFixClassPath) ? $strFixClassPath : null);
        $result = (
            ($repository instanceof AttributeEntityCollectionRepository) &&
            (
                is_null($strFixClassPath) ||
                ($repository instanceof $strFixClassPath)
            )
        );

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static($repository);
        }
		
		// Return result
		return $result;
    }
	
	
	
}