<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\attribute_model\provider\exception;

use Exception;

use liberty_code\attribute_model\provider\library\ConstRepoAttrProvider;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstRepoAttrProvider::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid array of attribute entity IDs, to load
            (
                (!isset($config[ConstRepoAttrProvider::TAB_CONFIG_KEY_LOAD_ID])) ||
                is_array($config[ConstRepoAttrProvider::TAB_CONFIG_KEY_LOAD_ID])
            ) &&

            // Check valid repository execution configuration
            (
                (!isset($config[ConstRepoAttrProvider::TAB_CONFIG_KEY_REPO_EXECUTION_CONFIG])) ||
                is_array($config[ConstRepoAttrProvider::TAB_CONFIG_KEY_REPO_EXECUTION_CONFIG])
            ) &&

            // Check valid cache attribute entity collection key
            (
                (!isset($config[ConstRepoAttrProvider::TAB_CONFIG_KEY_CACHE_ATTRIBUTE_ENTITY_COLLECTION_KEY])) ||
                (
                    is_string($config[ConstRepoAttrProvider::TAB_CONFIG_KEY_CACHE_ATTRIBUTE_ENTITY_COLLECTION_KEY]) &&
                    (trim($config[ConstRepoAttrProvider::TAB_CONFIG_KEY_CACHE_ATTRIBUTE_ENTITY_COLLECTION_KEY]) != '')
                )
            );

        // Return result
        return $result;
    }



    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     * @throws static
     */
    public static function setCheck($config)
    {
        // Init var
        $result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static((is_array($config) ? serialize($config) : $config));
        }

        // Return result
        return $result;
    }
	
	
	
}