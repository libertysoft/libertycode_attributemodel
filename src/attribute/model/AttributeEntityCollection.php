<?php
/**
 * This class allows to define attribute entity collection class.
 * key: attribute key => attribute entity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\attribute_model\attribute\model;

use liberty_code\model\entity\fix\model\FixEntityCollection;
use liberty_code\handle_model\attribute\api\AttributeCollectionInterface;

use liberty_code\model\entity\exception\CollectionValueInvalidFormatException;
use liberty_code\handle_model\attribute\library\ToolBoxEntity;
use liberty_code\handle_model\attribute\api\AttributeInterface;
use liberty_code\attribute_model\attribute\library\ConstAttribute;
use liberty_code\attribute_model\attribute\model\AttributeEntity;
use liberty_code\attribute_model\attribute\exception\AttributeKeyNotFoundException;



/**
 * @method null|AttributeEntity getItem(string $strKey) @inheritdoc
 * @method string setItem(AttributeEntity $objEntity) @inheritdoc
 */
class AttributeEntityCollection extends FixEntityCollection implements AttributeCollectionInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkAttributeExists($strKey)
    {
        // Return result
        return (!is_null($this->getObjAttribute($strKey)));
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return AttributeEntity::class;
    }



    /**
     * Get sorted associative array of items.
     * Format: key => value: item.
     *
     * Configuration format: @see getTabItem()
     *
     * @param array $tabConfig = null
     * @return array
     */
    public function getTabSortItem(array $tabConfig = null)
    {
        // Init var
        $result = $this->getTabItem($tabConfig);

        // Sort
        uasort(
            $result,
            function(AttributeEntity $objAttribute1, AttributeEntity $objAttribute2)
            {
                // Set check attribute orders valid
                $objAttribute1->setAttributeValid(ConstAttribute::ATTRIBUTE_KEY_ORDER);
                $objAttribute2->setAttributeValid(ConstAttribute::ATTRIBUTE_KEY_ORDER);

                // Get orders
                $intOrder1 = $objAttribute1->getAttributeValue(ConstAttribute::ATTRIBUTE_KEY_ORDER);
                $intOrder2 = $objAttribute2->getAttributeValue(ConstAttribute::ATTRIBUTE_KEY_ORDER);

                // Get comparison analysis
                $result = ($intOrder1 - $intOrder2);

                return $result;
            }
        );

        // Return result
        return $result;
    }



    /**
     * Get attribute entity collection key,
     * from specified attribute key.
     *
     * @param string $strAttributeKey
     * @param boolean $boolThrow = true
     * @return null|string
     * @throws AttributeKeyNotFoundException
     */
    protected function getStrKeyFromAttributeKey($strAttributeKey, $boolThrow = true)
    {
        // Init var
        $result = null;
        $tabKey = $this->getTabKey();

        // Run each entities
        for($intCpt = 0; ($intCpt < count($tabKey)) && is_null($result); $intCpt++)
        {
            $strKey = $tabKey[$intCpt];
            $objAttributeEntity = $this->getItem($strKey);

            // Register key, if found
            $result = (
                (
                    is_string($strAttributeKey) &&
                    ($objAttributeEntity->getStrAttributeKey() == $strAttributeKey)
                ) ?
                    $strKey :
                    null
            );
        }

        // Throw exception, if required
        if(is_null($result) && $boolThrow)
        {
            throw new AttributeKeyNotFoundException($strAttributeKey);
        }

        // Return result
        return $result;
    }



    /**
     * Get attribute key,
     * from specified attribute entity collection key.
     *
     * @param string $strKey
     * @return null|string
     */
    protected function getStrAttributeKeyFromKey($strKey)
    {
        // Init var
        $result = null;
        $objAttributeEntity = $this->getItem($strKey);
        $result = (
            (!is_null($objAttributeEntity)) ?
                $objAttributeEntity->getStrAttributeKey() :
                null
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabAttributeKey()
    {
        // Init var
        $result = $this->getTabKey();
        $result = array_map(
            function($strKey)
            {
                return $this->getStrAttributeKeyFromKey($strKey);
            },
            $result
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @return null|AttributeEntity
     */
    public function getObjAttribute($strKey)
    {
        // Init var
        $strKey = $this->getStrKeyFromAttributeKey($strKey, false);
        $result = (
            (!is_null($strKey)) ?
                $this->getItem($strKey) :
                null
        );

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param AttributeEntity $objAttribute
     * @throws CollectionValueInvalidFormatException
     */
    public function setAttribute(AttributeInterface $objAttribute)
    {
        // Check argument
        if(!($objAttribute instanceof AttributeEntity))
        {
            throw new CollectionValueInvalidFormatException($objAttribute);
        }

        // Init var
        /** @var AttributeEntity $objRole */
        $strKey = $this->setItem($objRole);
        $result = $this->getStrAttributeKeyFromKey($strKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @param array|static $tabAttribute
     */
    public function setTabAttribute($tabAttribute)
    {
        // Init var
        $tabKey = $this->setTabItem($tabAttribute);
        $result = array_map(
            function($strKey)
            {
                return $this->getStrAttributeKeyFromKey($strKey);
            },
            $tabKey
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @return AttributeEntity
     */
    public function removeAttribute($strKey)
    {
        // Init var
        $strKey = $this->getStrKeyFromAttributeKey($strKey);

        // return result
        return $this->removeItem($strKey);
    }



    /**
     * @inheritdoc
     */
    public function removeAttributeAll()
    {
        $this->removeItemAll();
    }





    // Methods entity attribute
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabEntityAttrConfig()
    {
        // Ini var
        $tabAttribute = array_values($this->getTabSortItem());
        $result = ToolBoxEntity::getTabAttributeConfig($tabAttribute);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabEntityAttrRuleConfig()
    {
        // Ini var
        $tabAttribute = array_values($this->getTabSortItem());
        $result = ToolBoxEntity::getTabAttributeRuleConfig($tabAttribute);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getEntityAttrValueFormatGet($strKey, $value)
    {
        // Return result
        return ToolBoxEntity::getAttributeValueFormatGet(
            $this,
            $strKey,
            $value
        );
    }



    /**
     * @inheritdoc
     */
    public function getEntityAttrValueFormatSet($strKey, $value)
    {
        // Return result
        return ToolBoxEntity::getAttributeValueFormatSet(
            $this,
            $strKey,
            $value
        );
    }



}