<?php
/**
 * This class allows to define attribute entity repository class.
 * Attribute entity repository allows to prepare data from attribute entity,
 * to save in persistence.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\attribute_model\attribute\model\repository;

use liberty_code\model\repository\multi\fix\model\FixMultiRepository;

use liberty_code\model\entity\repository\api\SaveEntityInterface;
use liberty_code\model\repository\library\ConstRepository;
use liberty_code\model\repository\sub_repository\library\ConstSubRepoRepository;
use liberty_code\model\repository\multi\library\ConstMultiRepository;
use liberty_code\model\datetime\library\ToolBoxDateTime;
use liberty_code\attribute_model\attribute\library\ConstAttribute;
use liberty_code\attribute_model\attribute\model\AttributeEntity;



abstract class AttributeEntityRepository extends FixMultiRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get fixed configuration array for persistence.
     * Overwrite it to implement specific configuration array.
     *
     * @return array
     */
    protected function getTabFixPersistorConfig()
    {
        // Return result
        return array();
    }



    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Init var
        $result = array_merge(
            array(
                ConstRepository::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => AttributeEntity::class,
                ConstMultiRepository::TAB_CONFIG_KEY_ATTRIBUTE_KEY_ID => ConstAttribute::ATTRIBUTE_KEY_ID,
                ConstSubRepoRepository::TAB_CONFIG_KEY_SEARCH_CRITERIA_ATTRIBUTE_KEY =>
                    ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_ID
            ),
            $this->getTabFixPersistorConfig()
        );

        // Return result
        return $result;
    }





    // Methods repository
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function save(
        SaveEntityInterface $objEntity,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Set check arguments
        $this->setCheckValidEntity($objEntity);

        // Update datetime create, update
        ToolBoxDateTime::hydrateEntityAttrDtCreateUpdate(
            $objEntity,
            ConstAttribute::ATTRIBUTE_KEY_DT_CREATE,
            ConstAttribute::ATTRIBUTE_KEY_DT_UPDATE
        );

        // Return result: call parent method
        return parent::save($objEntity, $tabConfig, $tabInfo);
    }



}


