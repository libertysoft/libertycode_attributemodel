<?php
/**
 * This class allows to define attribute entity collection repository class.
 * Attribute entity collection repository allows to prepare data from attribute entity collection,
 * to save in persistence.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\attribute_model\attribute\model\repository;

use liberty_code\model\repository\multi\fix\model\FixMultiCollectionRepository;

use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\datetime\library\ToolBoxDateTime;
use liberty_code\attribute_model\attribute\library\ConstAttribute;
use liberty_code\attribute_model\attribute\model\AttributeEntityFactory;
use liberty_code\attribute_model\attribute\model\repository\AttributeEntityRepository;



/**
 * @method null|AttributeEntityFactory getObjEntityFactory() @inheritdoc
 * @method null|AttributeEntityRepository getObjRepository() @inheritdoc
 */
abstract class AttributeEntityCollectionRepository extends FixMultiCollectionRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param AttributeEntityFactory $objEntityFactory
     * @param AttributeEntityRepository $objRepository
     */
    public function __construct(
        AttributeEntityFactory $objEntityFactory,
        AttributeEntityRepository $objRepository
    )
    {
        // Call parent constructor
        parent::__construct(
            $objEntityFactory,
            $objRepository
        );
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixEntityFactoryClassPath()
    {
        // Return result
        return AttributeEntityFactory::class;
    }



    /**
     * @inheritdoc
     */
    protected function getStrFixRepositoryClassPath()
    {
        // Return result
        return AttributeEntityRepository::class;
    }





    // Methods repository
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function save(
        EntityCollectionInterface $objCollection,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Set check arguments
        $this->setCheckValidCollection($objCollection);

        // Update datetime create, update, on entities
        ToolBoxDateTime::hydrateEntityCollectionAttrDtCreateUpdate(
            $objCollection,
            ConstAttribute::ATTRIBUTE_KEY_DT_CREATE,
            ConstAttribute::ATTRIBUTE_KEY_DT_UPDATE
        );

        // Return result: call parent method
        return parent::save($objCollection, $tabConfig, $tabInfo);
    }



}