<?php
/**
 * This class allows to define attribute entity factory class.
 * Attribute entity factory allows to design an attribute factory class,
 * to provide new attribute entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\attribute_model\attribute\model;

use liberty_code\model\entity\factory\fix\model\FixEntityFactory;
use liberty_code\handle_model\attribute\factory\api\AttributeFactoryInterface;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\handle_model\attribute\specification\api\AttrSpecInterface;
use liberty_code\handle_model\attribute\api\AttributeInterface;
use liberty_code\attribute_model\attribute\library\ConstAttribute;
use liberty_code\attribute_model\attribute\model\AttributeEntity;



/**
 * @method AttributeEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 */
class AttributeEntityFactory extends FixEntityFactory implements AttributeFactoryInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => AttributeEntity::class
        );
    }



    /**
     * @inheritdoc
     * @return AttributeEntity
     */
    protected function getObjEntityNew(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $objDateTimeFactory = $this->getObjInstance(DateTimeFactoryInterface::class);
        $objAttrSpec = $this->getObjInstance(AttrSpecInterface::class);
        $result = new AttributeEntity(
            array(),
            $objValidator,
            $objDateTimeFactory,
            $objAttrSpec
        );

        // Return result
        return $result;
    }



    /**
     * Get formatted configuration array.
     * Overwrite it to set specific feature.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return array
     */
    protected function getTabConfigFormat(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $result = $tabConfig;

        // Add configured key as attribute name, if required
        if(
            (!is_null($strConfigKey)) &&
            (!array_key_exists(ConstAttribute::ATTRIBUTE_KEY_NAME, $result))
        )
        {
            $result[ConstAttribute::ATTRIBUTE_KEY_NAME] = $strConfigKey;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrAttributeClassPath(array $tabConfig, $strConfigKey = null)
    {
        // Return result
        return $this->getStrEntityClassPath();
    }



    /**
     * @inheritdoc
     * @return AttributeEntity
     */
    public function getObjAttribute(
        array $tabConfig,
        $strConfigKey = null,
        AttributeInterface $objAttribute = null
    )
    {
        // Init var
        $strClassPath = $this->getStrEntityClassPath();
        $result = (
            is_null($objAttribute) ?
                $this->getObjEntity() :
                (
                    (
                        is_subclass_of($objAttribute, $strClassPath) ||
                        get_class($objAttribute) == $strClassPath
                    ) ?
                        $objAttribute :
                        null
                )
        );

        // Hydrate attribute entity, if required
        if(!is_null($result))
        {
            $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
            $result->setAttributeConfig($tabConfigFormat);
        }

        // Return result
        return $result;
    }



}