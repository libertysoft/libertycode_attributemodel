<?php
/**
 * This class allows to define attribute entity class.
 * Attribute entity allows to design an attribute class,
 * using entity.
 *
 * Attribute entity uses the following specified configuration:
 * [
 *     @see AttributeEntity::hydrate():
 *         List of attributes hydration: @see AttributeEntity::getTabConfig()
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\attribute_model\attribute\model;

use liberty_code\model\entity\repository\model\SaveConfigEntity;
use liberty_code\handle_model\attribute\api\AttributeInterface;

use DateTime;
use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\validation\validator\exception\RuleConfigInvalidFormatException;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\handle_model\attribute\specification\api\AttrSpecInterface;
use liberty_code\attribute_model\attribute\library\ConstAttribute;



/**
 * @property integer $intAttrId
 * @property string|DateTime $attrDtCreate
 * @property string|DateTime $attrDtUpdate
 * @property string $strAttrName
 * @property null|string $strAttrAlias (only if attribute alias required)
 * @property string $strAttrDataType
 * @property boolean $boolAttrValueRequired
 * @property array $tabAttrListValue
 * @property array $tabAttrRuleConfig
 * @property mixed $attrDefaultValue
 * @property integer $intAttrOrder
 */
class AttributeEntity extends SaveConfigEntity implements AttributeInterface
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * Datetime factory instance.
     * @var null|DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;



    /**
     * Attribute specification object
     * @var null|AttrSpecInterface
     */
    protected $objAttrSpec;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DateTimeFactoryInterface $objDateTimeFactory = null
     * @param AttrSpecInterface $objAttrSpec = null
     */
    public function __construct(
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        DateTimeFactoryInterface $objDateTimeFactory = null,
        AttrSpecInterface $objAttrSpec = null
    )
    {
        // Call parent constructor
        parent::__construct($tabValue, $objValidator);

        // Init datetime factory repository
        $this->setDateTimeFactory($objDateTimeFactory);

        // Init attribute specification
        $this->setAttrSpec($objAttrSpec);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
     * Check if attribute alias required.
     * Overwrite it to set specific option.
     *
     * @return boolean
     */
    public function checkAttrAliasRequired()
    {
        // Return result
        return true;
    }



    /**
     * @inheritdoc
     */
    protected function getStrBaseKey()
    {
        // Init var
        $result = $this->getAttributeValue(ConstAttribute::ATTRIBUTE_KEY_ID);
        $result = (is_null($result) ? parent::getStrBaseKey() : $result);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Init var
        $objDtFactory = $this->getObjDateTimeFactory();
        $objDtNow = new DateTime();
        $objDtNow = (
            (!is_null($objDtFactory)) ?
                $objDtFactory->getObjRefDt($objDtNow) :
                $objDtNow
        );
        $boolAttrAliasRequired = $this->checkAttrAliasRequired();
        $result = array(
            // Attribute id
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAttribute::ATTRIBUTE_KEY_ID,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAttribute::ATTRIBUTE_ALIAS_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute datetime create
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAttribute::ATTRIBUTE_KEY_DT_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAttribute::ATTRIBUTE_ALIAS_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => $objDtNow
            ],

            // Attribute datetime update
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAttribute::ATTRIBUTE_KEY_DT_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAttribute::ATTRIBUTE_ALIAS_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => $objDtNow
            ],

            // Attribute name
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAttribute::ATTRIBUTE_KEY_NAME,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAttribute::ATTRIBUTE_ALIAS_NAME,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_NM,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => ''
            ],

            // Attribute data type
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAttribute::ATTRIBUTE_KEY_DATA_TYPE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAttribute::ATTRIBUTE_ALIAS_DATA_TYPE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_DATA_TYPE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => ''
            ],

            // Attribute value required
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAttribute::ATTRIBUTE_KEY_VALUE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAttribute::ATTRIBUTE_ALIAS_VALUE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_VALUE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => false
            ],

            // Attribute list value
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAttribute::ATTRIBUTE_KEY_LIST_VALUE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAttribute::ATTRIBUTE_ALIAS_LIST_VALUE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_LIST_VALUE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => array()
            ],

            // Attribute rule configuration
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAttribute::ATTRIBUTE_KEY_RULE_CONFIG,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAttribute::ATTRIBUTE_ALIAS_RULE_CONFIG,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_RULE_CNF,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => array()
            ],

            // Attribute default value
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAttribute::ATTRIBUTE_KEY_DEFAULT_VALUE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAttribute::ATTRIBUTE_ALIAS_DEFAULT_VALUE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_DEFAULT_VALUE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute order
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAttribute::ATTRIBUTE_KEY_ORDER,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAttribute::ATTRIBUTE_ALIAS_ORDER,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_ORDER,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => 0
            ]
        );

        // Set attribute alias after name, if required
        if($boolAttrAliasRequired)
        {
            array_splice($result, 4, 0, array(
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAttribute::ATTRIBUTE_KEY_ALIAS,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAttribute::ATTRIBUTE_ALIAS_ALIAS,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_ALIAS,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ]
            ));
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init var
        $objAttrSpec = $this->getObjAttrSpec();
        $boolAttrAliasRequired = $this->checkAttrAliasRequired();
        $result = array(
            ConstAttribute::ATTRIBUTE_KEY_ID => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-integer' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    [
                                        'compare_value' => 0,
                                        'equal_enable_require' => false
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null or a strict positive integer.'
                    ]
                ]
            ],
            ConstAttribute::ATTRIBUTE_KEY_DT_CREATE => [
                'type_date'
            ],
            ConstAttribute::ATTRIBUTE_KEY_DT_UPDATE => [
                'type_date'
            ],
            ConstAttribute::ATTRIBUTE_KEY_NAME => [
                'type_string',
                [
                    'sub_rule_not',
                    [
                        'rule_config' => ['is_empty'],
                        'error_message_pattern' => '%1$s is empty.'
                    ]
                ]
            ],
            ConstAttribute::ATTRIBUTE_KEY_DATA_TYPE => (
                is_null($objAttrSpec) ?
                    [
                        'type_string',
                        [
                            'sub_rule_not',
                            [
                                'rule_config' => ['is_empty'],
                                'error_message_pattern' => '%1$s is empty.'
                            ]
                        ]
                    ] :
                    [
                        'type_string',
                        [
                            'sub_rule_not',
                            [
                                'rule_config' => ['is_empty'],
                                'error_message_pattern' => '%1$s is empty.'
                            ]
                        ],
                        [
                            'compare_in',
                            [
                                'compare_value' => $objAttrSpec->getTabDataType()
                            ]
                        ]
                    ]
            ),
            ConstAttribute::ATTRIBUTE_KEY_VALUE_REQUIRED => [
                [
                    'type_boolean',
                    [
                        'integer_enable_require' => false,
                        'string_enable_require' => false
                    ]
                ]
            ],
            ConstAttribute::ATTRIBUTE_KEY_LIST_VALUE => [
                'type_array',
                [
                    'sub_rule_iterate',
                    [
                        'rule_config' => [
                            [
                                'group_sub_rule_or',
                                [
                                    'rule_config' => [
                                        'is-null' => ['is_null'],
                                        'is-valid-string' => ['type_string'],
                                        'is-valid-numeric' => [
                                            [
                                                'type_numeric',
                                                [
                                                    'string_enable_require' => false
                                                ]
                                            ]
                                        ],
                                        'is-valid-boolean' => [
                                            [
                                                'type_boolean',
                                                [
                                                    'integer_enable_require' => false,
                                                    'string_enable_require' => false
                                                ]
                                            ]
                                        ],
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be an empty array or an array of null, string, numeric or boolean values.'
                    ]
                ]
            ],
            ConstAttribute::ATTRIBUTE_KEY_RULE_CONFIG => [
                [
                    'type_array',
                    [
                        'index_only_require' => true
                    ]
                ],
                [
                    'callable',
                    [
                        'valid_callable' => function($strName, $value) {
                            return RuleConfigInvalidFormatException::checkConfigIsValid($value);
                        },
                        'error_message_pattern' => '%1$s must be a valid rule configuration array.'
                    ]
                ]
            ],
            ConstAttribute::ATTRIBUTE_KEY_ORDER => [
                [
                    'type_numeric',
                    ['integer_only_require' => true]
                ],
                [
                    'compare_greater',
                    [
                        'compare_value' => 0,
                        'equal_enable_require' => true
                    ]
                ]
            ]
        );

        // Set attribute alias rule configuration, if required
        if($boolAttrAliasRequired)
        {
            $result[ConstAttribute::ATTRIBUTE_KEY_ALIAS] = array(
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-string' => [
                                'type_string',
                                [
                                    'sub_rule_not',
                                    [
                                        'rule_config' => ['is_empty'],
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null or a string not empty.'
                    ]
                ]
            );
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->getObjDateTimeFactory();

        // Format by attribute
        switch($strKey)
        {
            case ConstAttribute::ATTRIBUTE_KEY_DT_CREATE:
            case ConstAttribute::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->getObjDateTimeFactory();
        $boolAttrAliasRequired = $this->checkAttrAliasRequired();

        // Format by attribute
        switch($strKey)
        {
            case ConstAttribute::ATTRIBUTE_KEY_ID:
            case ConstAttribute::ATTRIBUTE_KEY_ORDER:
                $result = (
                    (is_string($value) && ctype_digit($value)) ?
                        intval($value) :
                        $value
                );
                break;

            case ConstAttribute::ATTRIBUTE_KEY_DT_CREATE:
            case ConstAttribute::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && is_string($value)) ?
                        $objDtFactory->getObjDtFromSet($value, true) :
                        (
                            ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                                $objDtFactory->getObjRefDt($value) :
                                $value
                        )
                );
                break;

            case ConstAttribute::ATTRIBUTE_KEY_ALIAS:
                $result = (
                    ($boolAttrAliasRequired && is_string($value) && (trim($value) === '')) ?
                        null :
                        $value
                );
                break;

            case ConstAttribute::ATTRIBUTE_KEY_VALUE_REQUIRED:
                $result = (
                    (
                        (
                            is_int($value) ||
                            (is_string($value) && ctype_digit($value))
                        ) &&
                        in_array(intval($value), array(0, 1))
                    ) ?
                        (intval($value) === 1) :
                        $value
                );
                break;

            case ConstAttribute::ATTRIBUTE_KEY_RULE_CONFIG:
                $result = ((is_array($value)) ? array_values($value) : $value);
                break;

            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->getObjDateTimeFactory();
        $boolAttrAliasRequired = $this->checkAttrAliasRequired();

        // Format by attribute
        switch($strKey)
        {
            case ConstAttribute::ATTRIBUTE_KEY_ID:
            case ConstAttribute::ATTRIBUTE_KEY_ORDER:
                $result = strval($value);
                break;

            case ConstAttribute::ATTRIBUTE_KEY_DT_CREATE:
            case ConstAttribute::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrSaveDt($value) :
                        $value
                );
                break;

            case ConstAttribute::ATTRIBUTE_KEY_ALIAS:
                $result = (($boolAttrAliasRequired && is_null($value)) ? '' : $value);
                break;

            case ConstAttribute::ATTRIBUTE_KEY_VALUE_REQUIRED:
                $result = ((is_bool($value) && $value) ? '1' : '0');
                break;

            case ConstAttribute::ATTRIBUTE_KEY_LIST_VALUE :
            case ConstAttribute::ATTRIBUTE_KEY_RULE_CONFIG :
                $result = (is_array($value) ? json_encode($value) : $value);
                break;

            case ConstAttribute::ATTRIBUTE_KEY_DEFAULT_VALUE :
                $result = json_encode($value);
                break;

            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->getObjDateTimeFactory();
        $boolAttrAliasRequired = $this->checkAttrAliasRequired();

        // Format by attribute
        switch($strKey)
        {
            case ConstAttribute::ATTRIBUTE_KEY_ID:
            case ConstAttribute::ATTRIBUTE_KEY_ORDER:
                $result = intval($value);
                break;

            case ConstAttribute::ATTRIBUTE_KEY_DT_CREATE:
            case ConstAttribute::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && is_string($value)) ?
                        $objDtFactory->getObjDtFromSave($value, true) :
                        (
                            ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                                $objDtFactory->getObjRefDt($value) :
                                $value
                        )
                );
                break;

            case ConstAttribute::ATTRIBUTE_KEY_ALIAS:
                $result = (($boolAttrAliasRequired && (trim($value) == '')) ? null : $value);
                break;

            case ConstAttribute::ATTRIBUTE_KEY_VALUE_REQUIRED:
                $result = ($value == '1');
                break;

            case ConstAttribute::ATTRIBUTE_KEY_LIST_VALUE :
            case ConstAttribute::ATTRIBUTE_KEY_RULE_CONFIG :
                $result = json_decode($value, true);
                $result = (is_array($result) ? $result : $value);
                break;

            case ConstAttribute::ATTRIBUTE_KEY_DEFAULT_VALUE :
                $result = json_decode($value, true);
                break;

            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get datetime factory object.
     *
     * @return null|DateTimeFactoryInterface
     */
    public function getObjDateTimeFactory()
    {
        // Return result
        return $this->objDateTimeFactory;
    }



    /**
     * Get attribute specification object.
     *
     * @return null|AttrSpecInterface
     */
    public function getObjAttrSpec()
    {
        // Return result
        return $this->objAttrSpec;
    }



    /**
     * Get formatted attribute configuration array.
     *
     * @param array $tabConfig
     * @return array
     */
    protected function getTabAttributeConfigFormat(array $tabConfig)
    {
        // Init var
        $result = array();
        $boolAttrAliasRequired = $this->checkAttrAliasRequired();

        // Map specific attributes
        $tabAttrKey = array(
            ConstAttribute::ATTRIBUTE_KEY_NAME,
            ConstAttribute::ATTRIBUTE_KEY_DATA_TYPE,
            ConstAttribute::ATTRIBUTE_KEY_VALUE_REQUIRED,
            ConstAttribute::ATTRIBUTE_KEY_LIST_VALUE,
            ConstAttribute::ATTRIBUTE_KEY_RULE_CONFIG,
            ConstAttribute::ATTRIBUTE_KEY_DEFAULT_VALUE,
            ConstAttribute::ATTRIBUTE_KEY_ORDER
        );
        if($boolAttrAliasRequired)
        {
            $tabAttrKey[] = ConstAttribute::ATTRIBUTE_KEY_ALIAS;
        }
        foreach($tabAttrKey as $strAttrKey)
        {
            if(array_key_exists($strAttrKey, $tabConfig))
            {
                $result[$strAttrKey] = $tabConfig[$strAttrKey];
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabAttributeConfig()
    {
        // Init var
        $result = $this->getTabData(false);
        $result = $this->getTabAttributeConfigFormat($result);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrAttributeKey()
    {
        // Init var
        $result = sprintf(
            ConstAttribute::CONF_ATTRIBUTE_KEY_PATTERN,
            strval($this->getStrBaseKey())
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabEntityAttrConfig()
    {
        // Init var
        $boolAttrAliasRequired = $this->checkAttrAliasRequired();

        // Init var
        $strAttributeKey = $this->getStrAttributeKey();
        $strAttributeName = $this->getAttributeValue(ConstAttribute::ATTRIBUTE_KEY_NAME);
        $strAttributeAlias = (
            $boolAttrAliasRequired ?
                $this->getAttributeValue(ConstAttribute::ATTRIBUTE_KEY_ALIAS) :
                null
        );
        $attributeDefaultValue = $this->getAttributeValue(ConstAttribute::ATTRIBUTE_KEY_DEFAULT_VALUE);
        $result = array(
            ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => $strAttributeKey,
            ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME => $strAttributeName,
            ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => (
                (!is_null($strAttributeAlias)) ?
                    $strAttributeAlias :
                    $strAttributeName
            ),
            ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => $attributeDefaultValue
        );

        // Return result
        return $result;
    }



    /**
     * Get string attribute data type.
     *
     * @return string
     */
    public function getStrAttributeDataType()
    {
        // Return result
        return $this->getAttributeValue(ConstAttribute::ATTRIBUTE_KEY_DATA_TYPE);
    }



    /**
     * Get entity attribute rule configuration array,
     * from attribute data type.
     *
     * Return array format:
     * @see AttrSpecInterface::getTabDataTypeRuleConfig() return array format.
     *
     * @return array
     */
    protected function getTabDataTypeRuleConfig()
    {
        // Init var
        $objAttrSpec = $this->getObjAttrSpec();
        $strDataType = $this->getStrAttributeDataType();
        $boolValueRequired = $this->getAttributeValue(ConstAttribute::ATTRIBUTE_KEY_VALUE_REQUIRED);
        $tabListValue = $this->getAttributeValue(ConstAttribute::ATTRIBUTE_KEY_LIST_VALUE);
        $tabListValue = (
            ToolBoxTable::checkTabIsIndex($tabListValue) ?
                array_values($tabListValue) :
                array_keys($tabListValue)
        );
        $result = $objAttrSpec->getTabDataTypeRuleConfig(
            $strDataType,
            $boolValueRequired,
            $tabListValue
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabEntityAttrRuleConfig()
    {
        // Init var
        $tabDataTypeRuleConfig = $this->getTabDataTypeRuleConfig();
        $tabRuleConfig = $this->getAttributeValue(ConstAttribute::ATTRIBUTE_KEY_RULE_CONFIG);
        $result = array_merge(
            $tabDataTypeRuleConfig,
            $tabRuleConfig
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getEntityAttrValueFormatGet($value)
    {
        // Init var
        $objAttrSpec = $this->getObjAttrSpec();
        $strDataType = $this->getStrAttributeDataType();
        $result = (
            (!is_null($objAttrSpec)) ?
                $objAttrSpec->getDataTypeValueFormatGet($strDataType, $value) :
                $value
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getEntityAttrValueFormatSet($value)
    {
        // Init var
        $objAttrSpec = $this->getObjAttrSpec();
        $strDataType = $this->getStrAttributeDataType();
        $result = (
            (!is_null($objAttrSpec)) ?
                $objAttrSpec->getDataTypeValueFormatSet($strDataType, $value) :
                $value
        );

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set datetime factory object.
     *
     * @param DateTimeFactoryInterface $objDateTimeFactory = null
     */
    public function setDateTimeFactory(DateTimeFactoryInterface $objDateTimeFactory = null)
    {
        $this->objDateTimeFactory = $objDateTimeFactory;
    }



    /**
     * Set attribute specification object.
     *
     * @param AttrSpecInterface $objAttrSpec = null
     */
    public function setAttrSpec(AttrSpecInterface $objAttrSpec = null)
    {
        // Set data
        $this->objAttrSpec = $objAttrSpec;
    }



    /**
     * @inheritdoc
     */
    public function setAttributeConfig(array $tabConfig)
    {
        // Init var
        $tabConfig = $this->getTabAttributeConfigFormat($tabConfig);

        // Hydrate attributes
        $this->hydrate($tabConfig);
    }



}