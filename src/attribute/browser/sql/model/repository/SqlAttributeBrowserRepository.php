<?php
/**
 * This class allows to define SQL attribute browser repository class.
 * SQL attribute browser repository uses SQL table browser,
 * to load attribute entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\attribute_model\attribute\browser\sql\model\repository;

use liberty_code\attribute_model\attribute\browser\model\repository\AttributeBrowserRepository;

use liberty_code\item_browser\browser\library\ConstBrowser;
use liberty_code\sql\browser\library\ConstBrowser as ConstSqlBrowser;
use liberty_code\sql\browser\table\model\TableBrowser;
use liberty_code\attribute_model\attribute\library\ConstAttribute;
use liberty_code\attribute_model\attribute\model\AttributeEntityCollection;
use liberty_code\attribute_model\attribute\sql\model\repository\SqlAttributeEntityCollectionRepository;
use liberty_code\attribute_model\attribute\browser\library\ConstAttributeBrowser;
use liberty_code\attribute_model\attribute\browser\sql\library\ConstSqlAttributeBrowser;



class SqlAttributeBrowserRepository extends AttributeBrowserRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * DI: Table browser instance.
     * @var TableBrowser
     */
    protected $objBrowser;




	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor / Others
	// ******************************************************************************

	/**
	 * @inheritdoc
	 * @param TableBrowser $objBrowser
     * @param SqlAttributeEntityCollectionRepository $objAttributeEntityCollectionRepo
     */
	public function __construct(
        TableBrowser $objBrowser,
        SqlAttributeEntityCollectionRepository $objAttributeEntityCollectionRepo
    )
	{
        // Init properties
        $this->objBrowser = $objBrowser;

		// Call parent constructor
		parent::__construct($objAttributeEntityCollectionRepo);
	}





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Call parent method
        parent::beanHydrateDefault();

        // Hydrate browser configuration
        $this->hydrateBrowserConfig();
    }



    /**
     * Hydrate configuration on browser object.
     */
    protected function hydrateBrowserConfig()
    {
        // hydrate browser configuration array
        $tabConfig = $this->getTabBrowserConfig();
        $this->objBrowser->setTabConfig($tabConfig);

        // Hydrate browser criteria operation configurations
        $tabCriteriaConfig = $this->getTabBrowserCriteriaConfig();
        $this->objBrowser->hydrateCriteriaConfig($tabCriteriaConfig, true, true);

        // Hydrate browser sort operation configurations
        $tabSortConfig = $this->getTabBrowserSortConfig();
        $this->objBrowser->hydrateSortConfig($tabSortConfig, true, true);
    }



    /**
     * Hydrate values on browser object,
     * from specified associative array of browser data.
     *
     * Browser data format: @see load()
     *
     * @param array $tabBrowserData
     * @return boolean
     */
    protected function hydrateBrowserValue(array $tabBrowserData)
    {
        // Build browser criteria operation values
        $tabAttrKey = array_keys($this->getTabBrowserCriteriaConfig());
        $tabValue = array();
        foreach($tabAttrKey as $strAttrKey)
        {
            // Register value, if required
            if(isset($tabBrowserData[$strAttrKey]))
            {
                $attrValue = $tabBrowserData[$strAttrKey];
                if(!is_null($attrValue))
                {
                    $attrValue = $this->getBrowserValueFormatCriteria($strAttrKey, $attrValue);
                    $tabValue[$strAttrKey] = $attrValue;
                }
            }
        }

        // Hydrate browser criteria operation values
        $result = $this->objBrowser->hydrateCriteriaValue($tabValue, true, true);

        // Build browser sort operation values
        $tabAttrKey = array_keys($this->getTabBrowserSortConfig());
        $tabValue = array();
        foreach($tabAttrKey as $strAttrKey)
        {
            // Register value, if required
            if(isset($tabBrowserData[$strAttrKey]))
            {
                $attrValue = $tabBrowserData[$strAttrKey];
                if(!is_null($attrValue))
                {
                    $attrValue = $this->getBrowserValueFormatSort($strAttrKey, $attrValue);
                    $tabValue[$strAttrKey] = $attrValue;
                }
            }
        }

        // Hydrate browser sort operation values
        $result = $this->objBrowser->hydrateSortValue($tabValue, true, true) && $result;

        // Hydrate browser count of item, per page
        if(isset($tabBrowserData[ConstAttributeBrowser::ATTRIBUTE_KEY_ITEM_COUNT_PAGE]))
        {
            $this->objBrowser->setItemCountPerPage($tabBrowserData[ConstAttributeBrowser::ATTRIBUTE_KEY_ITEM_COUNT_PAGE]);
        }

        // Hydrate browser index of active page
        if(isset($tabBrowserData[ConstAttributeBrowser::ATTRIBUTE_KEY_PAGE_INDEX]))
        {
            $this->objBrowser->setActivePageIndex($tabBrowserData[ConstAttributeBrowser::ATTRIBUTE_KEY_PAGE_INDEX]);
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if browser operation configurations required,
     * about alias criteria and sorting.
     * Overwrite it to set specific option.
     *
     * @return boolean
     */
    public function checkBrowserConfigAliasRequired()
    {
        // Init var
        /** @var SqlAttributeEntityCollectionRepository $objAttributeEntityCollectionRepo */
        $objAttributeEntityCollectionRepo = $this->objAttributeEntityCollectionRepo;
        $result = $objAttributeEntityCollectionRepo
            ->getObjRepository()
            ->checkStructureAliasRequired();

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get browser configuration array.
     *
     * @return array
     */
    protected function getTabBrowserConfig()
    {
        // Init var
        /** @var SqlAttributeEntityCollectionRepository $objAttributeEntityCollectionRepo */
        $objAttributeEntityCollectionRepo = $this->objAttributeEntityCollectionRepo;
        $strTableNm = $objAttributeEntityCollectionRepo
            ->getObjRepository()
            ->getStrSqlTableName();
        $result = array(
            ConstBrowser::TAB_CONFIG_KEY_QUERY => [
                'select' => [
                    ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_ID
                ],
                'from' => [
                    $strTableNm
                ]
            ]
        );

        // Return result
        return $result;
    }



    /**
     * Get browser criteria operation configurations array.
     * Overwrite it to set specific feature.
     *
     * @return array
     */
    protected function getTabBrowserCriteriaConfig()
    {
        // Init var
        $boolBrwConfigAliasRequired = $this->checkBrowserConfigAliasRequired();
        $result = array(
            ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID => [
                ConstSqlBrowser::TAB_OPERATION_CONFIG_KEY_CLAUSE => [
                    'operand' => ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_ID,
                    'operator' => 'equal',
                    'value' => ['value' => ConstSqlBrowser::OPERATION_TAG_VALUE]
                ]
            ],
            ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_IN_ID => [
                ConstSqlBrowser::TAB_OPERATION_CONFIG_KEY_CLAUSE => [
                    'operand' => ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_ID,
                    'operator' => 'in',
                    'value' => ['pattern' => ConstSqlBrowser::OPERATION_TAG_VALUE]
                ]
            ],
            ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_NAME => [
                ConstSqlBrowser::TAB_OPERATION_CONFIG_KEY_CLAUSE => [
                    'operand' => ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_NM,
                    'operator' => 'like',
                    'value' => ['value' => '%' . ConstSqlBrowser::OPERATION_TAG_VALUE . '%']
                ]
            ],
            ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_NAME => [
                ConstSqlBrowser::TAB_OPERATION_CONFIG_KEY_CLAUSE => [
                    'operand' => ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_NM,
                    'operator' => 'equal',
                    'value' => ['value' => ConstSqlBrowser::OPERATION_TAG_VALUE]
                ]
            ],
            ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_IN_NAME => [
                ConstSqlBrowser::TAB_OPERATION_CONFIG_KEY_CLAUSE => [
                    'operand' => ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_NM,
                    'operator' => 'in',
                    'value' => ['pattern' => ConstSqlBrowser::OPERATION_TAG_VALUE]
                ]
            ],
            ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_DATA_TYPE => [
                ConstSqlBrowser::TAB_OPERATION_CONFIG_KEY_CLAUSE => [
                    'operand' => ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_DATA_TYPE,
                    'operator' => 'equal',
                    'value' => ['value' => ConstSqlBrowser::OPERATION_TAG_VALUE]
                ]
            ],
            ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_IN_DATA_TYPE => [
                ConstSqlBrowser::TAB_OPERATION_CONFIG_KEY_CLAUSE => [
                    'operand' => ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_DATA_TYPE,
                    'operator' => 'in',
                    'value' => ['pattern' => ConstSqlBrowser::OPERATION_TAG_VALUE]
                ]
            ],
            ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE => [
                ConstSqlBrowser::TAB_OPERATION_CONFIG_KEY_CLAUSE => [
                    'operand' => ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_DT_CREATE,
                    'operator' => 'greater_equal',
                    'value' => ['value' => ConstSqlBrowser::OPERATION_TAG_VALUE]
                ]
            ],
            ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE => [
                ConstSqlBrowser::TAB_OPERATION_CONFIG_KEY_CLAUSE => [
                    'operand' => ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_DT_CREATE,
                    'operator' => 'less_equal',
                    'value' => ['value' => ConstSqlBrowser::OPERATION_TAG_VALUE]
                ]
            ],
            ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE => [
                ConstSqlBrowser::TAB_OPERATION_CONFIG_KEY_CLAUSE => [
                    'operand' => ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_DT_UPDATE,
                    'operator' => 'greater_equal',
                    'value' => ['value' => ConstSqlBrowser::OPERATION_TAG_VALUE]
                ]
            ],
            ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE => [
                ConstSqlBrowser::TAB_OPERATION_CONFIG_KEY_CLAUSE => [
                    'operand' => ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_DT_UPDATE,
                    'operator' => 'less_equal',
                    'value' => ['value' => ConstSqlBrowser::OPERATION_TAG_VALUE]
                ]
            ]
        );

        // Set browser criteria operation configurations about alias, if required
        if($boolBrwConfigAliasRequired)
        {
            $result = array_merge(
                $result,
                array(
                    ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_ALIAS => [
                        ConstSqlBrowser::TAB_OPERATION_CONFIG_KEY_CLAUSE => [
                            'operand' => ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_ALIAS,
                            'operator' => 'like',
                            'value' => ['value' => '%' . ConstSqlBrowser::OPERATION_TAG_VALUE . '%']
                        ]
                    ],
                    ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ALIAS => [
                        ConstSqlBrowser::TAB_OPERATION_CONFIG_KEY_CLAUSE => [
                            'operand' => ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_ALIAS,
                            'operator' => 'equal',
                            'value' => ['value' => ConstSqlBrowser::OPERATION_TAG_VALUE]
                        ]
                    ],
                    ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_IN_ALIAS => [
                        ConstSqlBrowser::TAB_OPERATION_CONFIG_KEY_CLAUSE => [
                            'operand' => ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_ALIAS,
                            'operator' => 'in',
                            'value' => ['pattern' => ConstSqlBrowser::OPERATION_TAG_VALUE]
                        ]
                    ]
                )
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get browser sort operation configurations array.
     * Overwrite it to set specific feature.
     *
     * @return array
     */
    protected function getTabBrowserSortConfig()
    {
        // Init var
        $boolBrwConfigAliasRequired = $this->checkBrowserConfigAliasRequired();
        $result = array(
            ConstAttributeBrowser::ATTRIBUTE_KEY_SORT_ID => [
                ConstSqlBrowser::TAB_OPERATION_CONFIG_KEY_CLAUSE => [
                    'operand' => ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_ID,
                    'operator' => ConstSqlBrowser::OPERATION_TAG_VALUE
                ]
            ],
            ConstAttributeBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE => [
                ConstSqlBrowser::TAB_OPERATION_CONFIG_KEY_CLAUSE => [
                    'operand' => ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_DT_CREATE,
                    'operator' => ConstSqlBrowser::OPERATION_TAG_VALUE
                ]
            ],
            ConstAttributeBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE => [
                ConstSqlBrowser::TAB_OPERATION_CONFIG_KEY_CLAUSE => [
                    'operand' => ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_DT_UPDATE,
                    'operator' => ConstSqlBrowser::OPERATION_TAG_VALUE
                ]
            ],
            ConstAttributeBrowser::ATTRIBUTE_KEY_SORT_NAME => [
                ConstSqlBrowser::TAB_OPERATION_CONFIG_KEY_CLAUSE => [
                    'operand' => ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_NM,
                    'operator' => ConstSqlBrowser::OPERATION_TAG_VALUE
                ]
            ],
            ConstAttributeBrowser::ATTRIBUTE_KEY_SORT_ORDER => [
                ConstSqlBrowser::TAB_OPERATION_CONFIG_KEY_CLAUSE => [
                    'operand' => ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_ORDER,
                    'operator' => ConstSqlBrowser::OPERATION_TAG_VALUE
                ]
            ]
        );

        // Set browser sort operation configurations about alias, if required
        if($boolBrwConfigAliasRequired)
        {
            $result = array_merge(
                $result,
                array(
                    ConstAttributeBrowser::ATTRIBUTE_KEY_SORT_ALIAS => [
                        ConstSqlBrowser::TAB_OPERATION_CONFIG_KEY_CLAUSE => [
                            'operand' => ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_ALIAS,
                            'operator' => ConstSqlBrowser::OPERATION_TAG_VALUE
                        ]
                    ]
                )
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get specified browser formatted value,
     * for criteria operation.
     * Overwrite it to implement specific format.
     *
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    protected function getBrowserValueFormatCriteria($strKey, $value)
    {
        // Init var
        $objConnection = $this->objBrowser->getObjCommandFactory()->getObjConnection();

        // Format by key
        switch($strKey)
        {
            default:
                $result = (
                is_array($value) ?
                    implode(
                        ', ',
                        array_map(
                            function($value) use ($objConnection) {
                                return $objConnection->getStrEscapeValue($value);
                            },
                            $value
                        )
                    ) :
                    $value
                );
                break;
        }

        // Return result
        return $result;
    }



    /**
     * Get specified browser formatted value,
     * for sort operation.
     * Overwrite it to implement specific format.
     *
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    protected function getBrowserValueFormatSort($strKey, $value)
    {
        // Format by key
        switch($strKey)
        {
            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabId(array $tabBrowserData)
    {
        // Init var
        $result = (
            // Get ids, if browser values hydration success
            $this->hydrateBrowserValue($tabBrowserData) ?
                array_map(
                    function($tabData) {
                        return array_values($tabData)[0];
                    },
                    $this->objBrowser->getTabItem()
                ) :
                null
        );

        // Return result
        return $result;
    }





    // Methods repository
    // ******************************************************************************

    /**
     * @inheritdoc
     *
     * Browser information returned format:
     * [
     *     parent browser information returned format (@see AttributeBrowserRepository::load() ) ,
     *
     *     item_count => integer (total count of item),
     *
     *     page_count => integer (total count of page)
     * ]
     */
    public function load(
        AttributeEntityCollection $objAttributeEntityCollection,
        array $tabBrowserData,
        array &$tabBrowserInfo = array()
    )
    {
        // Init var
        $result = parent::load($objAttributeEntityCollection, $tabBrowserData);
        $tabBrowserInfo = (
            $result ?
                array(
                    ConstSqlAttributeBrowser::TAB_BROWSE_INFO_KEY_ITEM_COUNT => $this->objBrowser->getIntItemCount(),
                    ConstAttributeBrowser::TAB_BROWSE_INFO_KEY_ITEM_COUNT_PAGE => $this->objBrowser->getIntItemCountPerPage(),
                    ConstAttributeBrowser::TAB_BROWSE_INFO_KEY_ITEM_COUNT_ACTIVE_PAGE => $this->objBrowser->getIntItemCountOnActivePage(),
                    ConstSqlAttributeBrowser::TAB_BROWSE_INFO_KEY_PAGE_COUNT => $this->objBrowser->getIntPageCount(),
                    ConstAttributeBrowser::TAB_BROWSE_INFO_KEY_PAGE_INDEX => $this->objBrowser->getIntActivePageIndex()
                ) :
                array()
        );

        // Return result
        return $result;
    }



}