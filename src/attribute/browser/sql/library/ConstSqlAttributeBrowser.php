<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\attribute_model\attribute\browser\sql\library;



class ConstSqlAttributeBrowser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************
    
    // Browser information configuration
    const TAB_BROWSE_INFO_KEY_ITEM_COUNT = 'item_count';
    const TAB_BROWSE_INFO_KEY_PAGE_COUNT = 'page_count';
}