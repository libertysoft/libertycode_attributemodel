<?php
/**
 * This class allows to define attribute browser entity class.
 * Attribute browser entity allows to define attributes,
 * to search attribute entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\attribute_model\attribute\browser\model;

use liberty_code\model\entity\model\ValidatorConfigEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\sql\database\command\clause\library\ConstOrderClause;
use liberty_code\handle_model\attribute\specification\api\AttrSpecInterface;
use liberty_code\attribute_model\attribute\browser\library\ConstAttributeBrowser;



/**
 * @property integer $intAttrItemCountPage
 * @property integer $intAttrPageIndex
 * @property null|integer $intAttrCritEqualId
 * @property null|integer[] $tabAttrCritInId
 * @property null|string $strAttrCritLikeName
 * @property null|string $strAttrCritEqualName
 * @property null|string[] $tabAttrCritInName
 * @property null|string $strAttrCritLikeAlias
 * @property null|string $strAttrCritEqualAlias
 * @property null|string[] $tabAttrCritInAlias
 * @property null|string $strAttrCritEqualDataType
 * @property null|string[] $tabAttrCritInDataType
 * @property null|string|DateTime $attrCritStartDtCreate
 * @property null|string|DateTime $attrCritEndDtCreate
 * @property null|string|DateTime $attrCritStartDtUpdate
 * @property null|string|DateTime $attrCritEndDtUpdate
 * @property null|string $strAttrSortId
 * @property null|string $strAttrSortDtCreate
 * @property null|string $strAttrSortDtUpdate
 * @property null|string $strAttrSortName
 * @property null|string $strAttrSortAlias
 * @property null|string $strAttrSortOrder
 */
class AttributeBrowserEntity extends ValidatorConfigEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * Datetime factory instance.
     * @var null|DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;



    /**
     * Attribute specification object
     * @var null|AttrSpecInterface
     */
    protected $objAttrSpec;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DateTimeFactoryInterface $objDateTimeFactory = null
     * @param AttrSpecInterface $objAttrSpec = null
     */
    public function __construct(
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        DateTimeFactoryInterface $objDateTimeFactory = null,
        AttrSpecInterface $objAttrSpec = null
    )
    {
        // Init attribute specification
        $this->setAttrSpec($objAttrSpec);

        // Init datetime factory repository
        $this->setDateTimeFactory($objDateTimeFactory);

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
     * Check if attributes alias required,
     * about criteria and sorting.
     * Overwrite it to set specific option.
     *
     * @return boolean
     */
    public function checkAttrAliasRequired()
    {
        // Return result
        return true;
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Init var
        $boolAttrAliasRequired = $this->checkAttrAliasRequired();
        $result = array(
            // Attribute page count
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAttributeBrowser::ATTRIBUTE_KEY_ITEM_COUNT_PAGE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAttributeBrowser::ATTRIBUTE_ALIAS_ITEM_COUNT_PAGE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => 10
            ],

            // Attribute page index
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAttributeBrowser::ATTRIBUTE_KEY_PAGE_INDEX,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAttributeBrowser::ATTRIBUTE_ALIAS_PAGE_INDEX,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => 0
            ],

            // Attribute criteria equal id
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAttributeBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_ID,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute criteria in id
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_IN_ID,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAttributeBrowser::ATTRIBUTE_ALIAS_CRIT_IN_ID,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute criteria like name
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_NAME,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAttributeBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_NAME,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute criteria equal name
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_NAME,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAttributeBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_NAME,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute criteria in name
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_IN_NAME,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAttributeBrowser::ATTRIBUTE_ALIAS_CRIT_IN_NAME,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute criteria equal data type
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_DATA_TYPE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAttributeBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_DATA_TYPE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute criteria in data type
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_IN_DATA_TYPE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAttributeBrowser::ATTRIBUTE_ALIAS_CRIT_IN_DATA_TYPE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute criteria start datetime create
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAttributeBrowser::ATTRIBUTE_ALIAS_CRIT_START_DT_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute criteria end datetime create
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAttributeBrowser::ATTRIBUTE_ALIAS_CRIT_END_DT_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute criteria start datetime update
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAttributeBrowser::ATTRIBUTE_ALIAS_CRIT_START_DT_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute criteria end datetime update
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAttributeBrowser::ATTRIBUTE_ALIAS_CRIT_END_DT_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute sort id
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAttributeBrowser::ATTRIBUTE_KEY_SORT_ID,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAttributeBrowser::ATTRIBUTE_ALIAS_SORT_ID,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute sort datetime create
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAttributeBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAttributeBrowser::ATTRIBUTE_ALIAS_SORT_DT_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute sort datetime update
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAttributeBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAttributeBrowser::ATTRIBUTE_ALIAS_SORT_DT_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute sort name
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAttributeBrowser::ATTRIBUTE_KEY_SORT_NAME,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAttributeBrowser::ATTRIBUTE_ALIAS_SORT_NAME,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute sort order
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAttributeBrowser::ATTRIBUTE_KEY_SORT_ORDER,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAttributeBrowser::ATTRIBUTE_ALIAS_SORT_ORDER,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ]
        );

        // Set attributes alias, if required
        if($boolAttrAliasRequired)
        {
            // Set attributes alias criteria after name criteria
            array_splice($result, 7, 0, array(
                // Attribute criteria like alias
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_ALIAS,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAttributeBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_ALIAS,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal alias
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ALIAS,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAttributeBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_ALIAS,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in alias
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_IN_ALIAS,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAttributeBrowser::ATTRIBUTE_ALIAS_CRIT_IN_ALIAS,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ]
            ));

            // Set attributes alias sorting after name sorting
            array_splice($result, -1, 0, array(
                // Attribute sort alias
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAttributeBrowser::ATTRIBUTE_KEY_SORT_ALIAS,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAttributeBrowser::ATTRIBUTE_ALIAS_SORT_ALIAS,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ]
            ));
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init var
        $objAttrSpec = $this->getObjAttrSpec();
        $boolAttrAliasRequired = $this->checkAttrAliasRequired();

        // Get rule configuration
        $tabRuleConfigNullValidInteger = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-integer' => [
                            [
                                'type_numeric',
                                ['integer_only_require' => true]
                            ],
                            [
                                'compare_greater',
                                [
                                    'compare_value' => 0,
                                    'equal_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a strict positive integer.'
                ]
            ]
        );

        $tabRuleConfigNullValidTabInteger = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-integer' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        [
                                            'type_numeric',
                                            ['integer_only_require' => true]
                                        ],
                                        [
                                            'compare_greater',
                                            [
                                                'compare_value' => 0,
                                                'equal_enable_require' => false
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strict positive integers.'
                ]
            ]
        );

        $tabRuleConfigNullValidString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-string' => [
                            'type_string',
                            [
                                'sub_rule_not',
                                [
                                    'rule_config' => ['is_empty'],
                                    'error_message_pattern' => '%1$s is empty.'
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a string not empty.'
                ]
            ]
        );

        $tabRuleConfigNullValidTabString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-string' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        'type_string',
                                        [
                                            'sub_rule_not',
                                            [
                                                'rule_config' => ['is_empty']
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strings not empty.'
                ]
            ]
        );

        $tabRuleConfigNullValidDateTime = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-date' => ['type_date']
                    ],
                    'error_message_pattern' => '%1$s must be null or a date.'
                ]
            ]
        );

        $tabDataType = (
            (!is_null($objAttrSpec)) ?
                $objAttrSpec->getTabDataType() :
                array()
        );
        $strTabDataType = implode(', ', array_map(
            function($value) {return sprintf('\'%1$s\'', $value);},
            $tabDataType
        ));

        $tabSort = array(
            ConstOrderClause::OPERATOR_CONFIG_ASC,
            ConstOrderClause::OPERATOR_CONFIG_DESC
        );
        $strTabSort = implode(', ', array_map(
            function($value) {return sprintf('\'%1$s\'', $value);},
            $tabSort
        ));
        $tabRuleConfigNullValidSortString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-sort' => [
                            [
                                'compare_in',
                                [
                                    'compare_value' => $tabSort
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a valid sort string (' . $strTabSort . ').'
                ]
            ]
        );

        $result = array(
            ConstAttributeBrowser::ATTRIBUTE_KEY_ITEM_COUNT_PAGE => [
                [
                    'type_numeric',
                    ['integer_only_require' => true]
                ],
                [
                    'compare_between',
                    [
                        'greater_compare_value' => 0,
                        'greater_equal_enable_require' => false,
                        'less_compare_value' => 100
                    ]
                ]
            ],
            ConstAttributeBrowser::ATTRIBUTE_KEY_PAGE_INDEX => [
                [
                    'type_numeric',
                    ['integer_only_require' => true]
                ],
                [
                    'compare_greater',
                    ['compare_value' => 0]
                ]
            ],
            ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID => $tabRuleConfigNullValidInteger,
            ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_IN_ID => $tabRuleConfigNullValidTabInteger,
            ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_NAME => $tabRuleConfigNullValidString,
            ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_NAME => $tabRuleConfigNullValidString,
            ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_IN_NAME => $tabRuleConfigNullValidTabString,
            ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_DATA_TYPE => array(
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-data-type' => [
                                [
                                    'compare_in',
                                    [
                                        'compare_value' => $tabDataType
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null or a valid data type string (' . $strTabDataType . ').'
                    ]
                ]
            ),
            ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_IN_DATA_TYPE => array(
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-data-type' => [
                                [
                                    'type_array',
                                    [
                                        'index_only_require' => true
                                    ]
                                ],
                                [
                                    'sub_rule_iterate',
                                    [
                                        'rule_config' => [
                                            [
                                                'compare_in',
                                                [
                                                    'compare_value' => $tabDataType
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null or an array of valid data type strings (' . $strTabDataType . ').'
                    ]
                ]
            ),
            ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE => $tabRuleConfigNullValidDateTime,
            ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE => $tabRuleConfigNullValidDateTime,
            ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE => $tabRuleConfigNullValidDateTime,
            ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE => $tabRuleConfigNullValidDateTime,
            ConstAttributeBrowser::ATTRIBUTE_KEY_SORT_ID => $tabRuleConfigNullValidSortString,
            ConstAttributeBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE => $tabRuleConfigNullValidSortString,
            ConstAttributeBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE => $tabRuleConfigNullValidSortString,
            ConstAttributeBrowser::ATTRIBUTE_KEY_SORT_NAME => $tabRuleConfigNullValidSortString,
            ConstAttributeBrowser::ATTRIBUTE_KEY_SORT_ORDER => $tabRuleConfigNullValidSortString
        );

        // Set attributes alias rule configurations, if required
        if($boolAttrAliasRequired)
        {
            $result = array_merge(
                $result,
                array(
                    ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_ALIAS => $tabRuleConfigNullValidString,
                    ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ALIAS => $tabRuleConfigNullValidString,
                    ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_IN_ALIAS => $tabRuleConfigNullValidTabString,
                    ConstAttributeBrowser::ATTRIBUTE_KEY_SORT_ALIAS => $tabRuleConfigNullValidSortString
                )
            );
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->getObjDateTimeFactory();

        // Format by attribute
        switch($strKey)
        {
            case ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->getObjDateTimeFactory();
        $boolAttrAliasRequired = $this->checkAttrAliasRequired();

        // Format by attribute
        switch($strKey)
        {
            case ConstAttributeBrowser::ATTRIBUTE_KEY_ITEM_COUNT_PAGE:
            case ConstAttributeBrowser::ATTRIBUTE_KEY_PAGE_INDEX:
                $result = (
                    (is_string($value) && ctype_digit($value)) ?
                        intval($value) :
                        $value
                );
                break;

            case ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_IN_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_array($value)) ?
                                array_values(array_map(
                                    function($value){
                                        return (
                                            (is_string($value) && ctype_digit($value)) ?
                                                intval($value) :
                                                $value
                                        );
                                    },
                                    $value
                                )) :
                                $value
                        )
                );
                break;

            case ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_NAME:
            case ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_NAME:
            case ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_DATA_TYPE:
            case ConstAttributeBrowser::ATTRIBUTE_KEY_SORT_ID:
            case ConstAttributeBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE:
            case ConstAttributeBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE:
            case ConstAttributeBrowser::ATTRIBUTE_KEY_SORT_NAME:
            case ConstAttributeBrowser::ATTRIBUTE_KEY_SORT_ORDER:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        $value
                );
                break;

            case ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_IN_NAME:
            case ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_IN_DATA_TYPE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        ((is_array($value)) ? array_values($value) : $value)
                );
                break;

            case ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_ALIAS:
            case ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ALIAS:
            case ConstAttributeBrowser::ATTRIBUTE_KEY_SORT_ALIAS:
                $result = (
                    ($boolAttrAliasRequired && is_string($value) && (trim($value) === '')) ?
                        null :
                        $value
                );
                break;

            case ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_IN_ALIAS:
                $result = (
                    ($boolAttrAliasRequired && is_string($value) && (trim($value) === '')) ?
                        null :
                        (($boolAttrAliasRequired && is_array($value)) ? array_values($value) : $value)
                );
                break;

            case ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            ((!is_null($objDtFactory)) && is_string($value)) ?
                                $objDtFactory->getObjDtFromSet($value, true) :
                                (
                                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                                        $objDtFactory->getObjRefDt($value) :
                                        $value
                                )
                        )
                );
                break;

            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get datetime factory object.
     *
     * @return null|DateTimeFactoryInterface
     */
    public function getObjDateTimeFactory()
    {
        // Return result
        return $this->objDateTimeFactory;
    }



    /**
     * Get attribute specification object.
     *
     * @return null|AttrSpecInterface
     */
    public function getObjAttrSpec()
    {
        // Return result
        return $this->objAttrSpec;
    }



    /**
     * Get array of browser data (Attribute key => attribute value).
     *
     * @return array
     */
    public function getTabBrowserData()
    {
        // Init var
        $result = array();
        $objDtFactory = $this->getObjDateTimeFactory();

        // Run all attribute keys
        foreach($this->getTabAttributeKey() as $strKey)
        {
            // Get attribute value
            switch($strKey)
            {
                case ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
                case ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
                case ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
                case ConstAttributeBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                    $value = $this->getAttributeValue($strKey, false);
                    $value = (
                        ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                            $objDtFactory->getStrSaveDt($value) :
                            $value
                    );
                    break;

                default:
                    $value = $this->getAttributeValue($strKey);
                    break;
            }

            // Register attribute in result
            $result[$strKey] = $value;
        }

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set datetime factory object.
     *
     * @param DateTimeFactoryInterface $objDateTimeFactory = null
     */
    public function setDateTimeFactory(DateTimeFactoryInterface $objDateTimeFactory = null)
    {
        $this->objDateTimeFactory = $objDateTimeFactory;
    }



    /**
     * Set attribute specification object.
     *
     * @param AttrSpecInterface $objAttrSpec = null
     */
    public function setAttrSpec(AttrSpecInterface $objAttrSpec = null)
    {
        // Set data
        $this->objAttrSpec = $objAttrSpec;
    }



}