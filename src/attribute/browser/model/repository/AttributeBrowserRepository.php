<?php
/**
 * This class allows to define attribute browser repository class.
 * Attribute browser repository allows to load attribute entities,
 * from specified attribute browser entity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\attribute_model\attribute\browser\model\repository;

use liberty_code\library\bean\model\DefaultBean;

use liberty_code\attribute_model\attribute\model\AttributeEntityCollection;
use liberty_code\attribute_model\attribute\model\repository\AttributeEntityCollectionRepository;
use liberty_code\attribute_model\attribute\browser\library\ConstAttributeBrowser;
use liberty_code\attribute_model\attribute\browser\model\AttributeBrowserEntity;



abstract class AttributeBrowserRepository extends DefaultBean
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * DI: Attribute entity collection repository instance.
     * @var AttributeEntityCollectionRepository
     */
    protected $objAttributeEntityCollectionRepo;




	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor / Others
	// ******************************************************************************

	/**
	 * @inheritdoc
     * @param AttributeEntityCollectionRepository $objAttributeEntityCollectionRepo
     */
	public function __construct(AttributeEntityCollectionRepository $objAttributeEntityCollectionRepo)
	{
        // Init properties
        $this->objAttributeEntityCollectionRepo = $objAttributeEntityCollectionRepo;

		// Call parent constructor
		parent::__construct();
	}





    // Methods getters
    // ******************************************************************************

    /**
     * Get index array of attribute ids,
     * from specified associative array of browser data.
     *
     * Browser data format: @see load()
     *
     * @param array $tabBrowserData
     * @return null|array
     */
    abstract protected function getTabId(array $tabBrowserData);





    // Methods repository
    // ******************************************************************************

    /**
     * Load specified attribute entity collection object,
     * from specified associative array of browser data.
     * Return true if all attribute entities success, false if an error occurs on at least one attribute entity.
     *
     * Browser data format:
     * [
     *     @see AttributeBrowserEntity attributes list
     * ]
     *
     * Browser information returned format:
     * [
     *     item_count_page => integer (count of item, per page),
     *
     *     item_count_active_page => integer (count of item, on active page),
     *
     *     page_index => integer (index of active page)
     * ]
     *
     * @param AttributeEntityCollection $objAttributeEntityCollection
     * @param array $tabBrowserData
     * @param array &$tabBrowserInfo = array()
     * @return boolean
     */
    public function load(
        AttributeEntityCollection $objAttributeEntityCollection,
        array $tabBrowserData,
        array &$tabBrowserInfo = array()
    )
    {
        // Init var
        $tabId = $this->getTabId($tabBrowserData);
        $result = (
            is_array($tabId) ?
                $this->objAttributeEntityCollectionRepo->load($objAttributeEntityCollection, $tabId) :
                false
        );
        $tabBrowserInfo = (
            $result ?
                array(
                    ConstAttributeBrowser::TAB_BROWSE_INFO_KEY_ITEM_COUNT_PAGE => (
                        isset($tabBrowserData[ConstAttributeBrowser::ATTRIBUTE_KEY_ITEM_COUNT_PAGE]) ?
                            $tabBrowserData[ConstAttributeBrowser::ATTRIBUTE_KEY_ITEM_COUNT_PAGE] :
                            0
                    ),
                    ConstAttributeBrowser::TAB_BROWSE_INFO_KEY_ITEM_COUNT_ACTIVE_PAGE => $objAttributeEntityCollection->count(),
                    ConstAttributeBrowser::TAB_BROWSE_INFO_KEY_PAGE_INDEX => (
                        isset($tabBrowserData[ConstAttributeBrowser::ATTRIBUTE_KEY_PAGE_INDEX]) ?
                            $tabBrowserData[ConstAttributeBrowser::ATTRIBUTE_KEY_PAGE_INDEX] :
                            0
                    )
                ) :
                array()
        );

        // Return result
        return $result;
    }



    /**
     * Load specified attribute entity collection object,
     * from specified attribute browser entity.
     * Return true if all attribute entities success, false if an error occurs on at least one attribute entity.
     *
     * Additive browser data format: @see load() browser data format.
     *
     * Browser information returned format: @see load() browser information format.
     *
     * @param AttributeEntityCollection $objAttributeEntityCollection
     * @param AttributeBrowserEntity $objAttributeBrowserEntity
     * @param array $tabAddBrowserData = array()
     * @param array &$tabBrowserInfo = array()
     * @return boolean
     */
    public function loadFromEntity(
        AttributeEntityCollection $objAttributeEntityCollection,
        AttributeBrowserEntity $objAttributeBrowserEntity,
        array $tabAddBrowserData = array(),
        array &$tabBrowserInfo = array()
    )
    {
        // Init var
        $tabBrowserData = array_merge(
            $objAttributeBrowserEntity->getTabBrowserData(),
            $tabAddBrowserData
        );
        $result = $this->load($objAttributeEntityCollection, $tabBrowserData, $tabBrowserInfo);

        // Return result
        return $result;
    }



}