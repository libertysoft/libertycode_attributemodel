<?php

// Init var
$strRepoRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRepoRootAppPath . '/src/attribute/browser/test/AttributeBrowserRepositoryTest.php');

$strGetClean = (isset($_GET['clean']) ? $_GET['clean'] : '0');
$_GET['clean'] = '0';
$_GET['clean_attribute'] = '0';
ob_start();
require_once($strRepoRootAppPath . '/src/attribute/test/Test.php');
ob_get_clean();
$objConnection->connect(); // Reconnect database (previously de-connected)

// Use
use liberty_code\attribute_model\attribute\repository\model\SaveAttributeEntityCollection;
use liberty_code\attribute_model\attribute\browser\model\AttributeBrowserEntity;



// Test browser load
$objDtNow = new DateTime('now', new DateTimeZone('America/New_York'));
$objDtStart = (clone $objDtNow)->sub(new DateInterval('P1D'));
$objDtEnd = (clone $objDtNow)->add(new DateInterval('P1D'));
$tabTabData = array(
    [
        'intAttrItemCountPage' => 1,
        'intAttrPageIndex' => 0,
        'strAttrSortName' => 'desc'
    ], // Ok: show attr-4
    [
        'intAttrItemCountPage' => 2,
        'intAttrPageIndex' => 0,
        'strAttrCritLikeName' => 'Attr',
        'strAttrSortName' => 'asc'
    ], // Ok: show attr-1, attr-2
    [
        'strAttrCritEqualAlias' => 'attr-3',
    ], // Ok: show attr-3
    [
        'intAttrItemCountPage' => 1,
        'intAttrPageIndex' => 1,
        'tabAttrCritInDataType' => ['integer', 'boolean'],
        'strAttrSortOrder' => 'asc'
    ], // Ok: show attr-2
    [
        'attrCritStartDtCreate' => $objDtNow->format('Y-m-d H:i:s.u'),
        'attrCritEndDtCreate' => $objDtNow->format('Y-m-d H:i:s.u'),
        'attrCritStartDtUpdate' => $objDtNow->format('Y-m-d H:i:s.u'),
        'attrCritEndDtUpdate' => $objDtNow->format('Y-m-d H:i:s.u')
    ], // Ok: show nothing
    [
        //'attrCritStartDtCreate' => $objDtStart->format('Y-m-d H:i:s.u'),
        //'attrCritEndDtCreate' => $objDtEnd->format('Y-m-d H:i:s.u'),
        'attrCritStartDtUpdate' => $objDtStart->format('Y-m-d H:i:s.u'),
        'attrCritEndDtUpdate' => $objDtEnd->format('Y-m-d H:i:s.u')
    ] // Ok: show attr-1, attr-2, attr-3, attr-4
);
echo('Test browser load: <br />');

foreach($tabTabData as $tabData)
{
    echo('Browser data: <pre>');
    print_r($tabData);
    echo('</pre>');

    try
    {
        // Set browser entity data
        $objAttributeBrowserEntity = new AttributeBrowserEntity(
            array(),
            $objValidator,
            $objDateTimeFactory,
            $objAttrSpec
        );
        $objAttributeBrowserEntity->hydrate($tabData);
        $objAttributeBrowserEntity->setAttributeValid();

        // Load attribute entities
        $objAttributeEntityCollection = new SaveAttributeEntityCollection();
        $tabBrowserInfo = array();
        $objAttributeBrowserRepo->loadFromEntity(
            $objAttributeEntityCollection,
            $objAttributeBrowserEntity,
            array(),
            $tabBrowserInfo
        );

        echo('Attribute entities: <pre>');
        foreach($objAttributeEntityCollection->getTabKey() as $strKey)
        {
            var_dump($objAttributeEntityCollection->getItem($strKey)->getTabData());
        }
        echo('</pre>');

        echo('<br />');

        echo('Browser info: <pre>');
        print_r($tabBrowserInfo);
        echo('</pre>');
    }
    catch(Exception $e)
    {
        echo(get_class($e) . ' - ' . $e->getMessage().'<br /><br />');
    }

    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Remove test database, if required
$_GET['clean'] = $strGetClean;
require_once($strRepoRootAppPath . '/src/attribute/test/db/HelpRemoveDbTest.php');


