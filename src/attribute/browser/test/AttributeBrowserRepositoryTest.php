<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/attribute/test/AttributeRepositoryTest.php');

// Use
use liberty_code\attribute_model\attribute\browser\sql\model\repository\SqlAttributeBrowserRepository;



// Init repository
$objAttributeBrowserRepo = new SqlAttributeBrowserRepository(
    $objTableBrowser,
    $objAttributeEntityCollectionRepo
);


