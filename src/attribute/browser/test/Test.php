<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/attribute/test/AttributeTest.php');

// Use
use liberty_code\attribute_model\attribute\browser\model\AttributeBrowserEntity;



// Init var
$objAttributeBrowserEntity = new AttributeBrowserEntity(
    array(),
    $objValidator,
    $objDateTimeFactory,
    $objAttrSpec
);



// Test set attribute
$objDtNow = new DateTime('now', new DateTimeZone('America/New_York'));
$tabTabData = array(
    [
        'intAttrItemCountPage' => 0,
        'intAttrPageIndex' => -3,
        'intAttrCritEqualId' => 'test',
        'tabAttrCritInId' => [1, 'test'],
        'strAttrCritLikeName' => 7,
        'strAttrCritEqualName' => '',
        'tabAttrCritInName' => ['', 'Test 2'],
        'strAttrCritLikeAlias' => 0,
        'strAttrCritEqualAlias' => '',
        'tabAttrCritInAlias' => ['', 'test-2'],
        'strAttrCritEqualDataType' => 'test',
        'tabAttrCritInDataType' => [7, 'test'],
        'attrCritStartDtCreate' => 'test',
        'attrCritEndDtCreate' => true,
        'attrCritStartDtUpdate' => false,
        'attrCritEndDtUpdate' => 7,
        'strAttrSortId' => 'test',
        'strAttrSortDtCreate' => 'test',
        'strAttrSortDtUpdate' => 'test',
        'strAttrSortName' => 7,
        'strAttrSortAlias' => 'test',
        'strAttrSortOrder' => true
    ],
    [
        'intAttrItemCountPage' => 50,
        'intAttrPageIndex' => 0,
        'intAttrCritEqualId' => 1,
        'tabAttrCritInId' => [1, 3],
        'strAttrCritLikeName' => 'Test',
        'strAttrCritEqualName' => 'Test',
        'tabAttrCritInName' => ['Test', 'Test 2'],
        'strAttrCritLikeAlias' => 'test',
        'strAttrCritEqualAlias' => 'test',
        'tabAttrCritInAlias' => ['test', 'test-2'],
        'strAttrCritEqualDataType' => 'string',
        'tabAttrCritInDataType' => ['integer', 'boolean'],
        'attrCritStartDtCreate' => $objDtNow->format('Y-m-d H:i:s.u'),
        'attrCritEndDtCreate' => $objDtNow->format('Y-m-d H:i:s.u'),
        'attrCritStartDtUpdate' => $objDtNow->format('Y-m-d H:i:s.u'),
        'attrCritEndDtUpdate' => $objDtNow->format('Y-m-d H:i:s.u'),
        'strAttrSortId' => 'asc',
        'strAttrSortDtCreate' => 'desc',
        'strAttrSortDtUpdate' => 'asc',
        'strAttrSortName' => 'desc',
        'strAttrSortAlias' => 'asc',
        'strAttrSortOrder' => 'asc'
    ],
);
echo('Test set attribute: <br />');

foreach($tabTabData as $tabData)
{
    echo('Set data: <pre>');
    print_r($tabData);
    echo('</pre>');

    try
    {
        $objAttributeBrowserEntity->hydrate($tabData);

        foreach($objAttributeBrowserEntity->getTabAttributeKey() as $strKey)
        {
            try
            {
                echo('Check "'.$strKey.'": <pre>');
                var_dump($objAttributeBrowserEntity->setAttributeValid($strKey));
                echo('</pre>');
            }
            catch(Exception $e)
            {
                echo(get_class($e) . ' - ' . $e->getMessage().'<br /><br />');
            }
        }
    }
    catch(Exception $e)
    {
        echo(get_class($e) . ' - ' . $e->getMessage().'<br /><br />');
    }
    finally
    {
        echo('Validation done!<br /><br />');
    }

    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test get attribute
echo('Test get attribute: <pre>');
print_r($objAttributeBrowserEntity->getTabData());
echo('</pre>');

echo('<br /><br /><br />');


