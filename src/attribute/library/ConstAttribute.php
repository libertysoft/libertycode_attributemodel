<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\attribute_model\attribute\library;



class ConstAttribute
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Attribute configuration
    const ATTRIBUTE_KEY_ID = 'intAttrId';
    const ATTRIBUTE_KEY_DT_CREATE = 'attrDtCreate';
    const ATTRIBUTE_KEY_DT_UPDATE = 'attrDtUpdate';
    const ATTRIBUTE_KEY_NAME = 'strAttrName';
    const ATTRIBUTE_KEY_ALIAS = 'strAttrAlias';
    const ATTRIBUTE_KEY_DATA_TYPE = 'strAttrDataType';
    const ATTRIBUTE_KEY_VALUE_REQUIRED = 'boolAttrValueRequired';
    const ATTRIBUTE_KEY_LIST_VALUE = 'tabAttrListValue';
    const ATTRIBUTE_KEY_RULE_CONFIG = 'tabAttrRuleConfig';
    const ATTRIBUTE_KEY_DEFAULT_VALUE = 'attrDefaultValue';
    const ATTRIBUTE_KEY_ORDER = 'intAttrOrder';

    const ATTRIBUTE_ALIAS_ID = 'id';
    const ATTRIBUTE_ALIAS_DT_CREATE = 'dt-create';
    const ATTRIBUTE_ALIAS_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_ALIAS_NAME = 'name';
    const ATTRIBUTE_ALIAS_ALIAS = 'alias';
    const ATTRIBUTE_ALIAS_DATA_TYPE = 'data-type';
    const ATTRIBUTE_ALIAS_VALUE_REQUIRED = 'value-required';
    const ATTRIBUTE_ALIAS_LIST_VALUE = 'list-value';
    const ATTRIBUTE_ALIAS_RULE_CONFIG = 'rule-config';
    const ATTRIBUTE_ALIAS_DEFAULT_VALUE = 'default-value';
    const ATTRIBUTE_ALIAS_ORDER = 'order';

    const ATTRIBUTE_NAME_SAVE_ATTR_ID = 'attr_id';
    const ATTRIBUTE_NAME_SAVE_ATTR_DT_CREATE = 'attr_dt_create';
    const ATTRIBUTE_NAME_SAVE_ATTR_DT_UPDATE = 'attr_dt_update';
    const ATTRIBUTE_NAME_SAVE_ATTR_NM = 'attr_nm';
    const ATTRIBUTE_NAME_SAVE_ATTR_ALIAS = 'attr_alias';
    const ATTRIBUTE_NAME_SAVE_ATTR_DATA_TYPE = 'attr_data_type';
    const ATTRIBUTE_NAME_SAVE_ATTR_VALUE_REQUIRED = 'attr_value_required';
    const ATTRIBUTE_NAME_SAVE_ATTR_LIST_VALUE = 'attr_list_value';
    const ATTRIBUTE_NAME_SAVE_ATTR_RULE_CNF = 'attr_rule_cnf';
    const ATTRIBUTE_NAME_SAVE_ATTR_DEFAULT_VALUE = 'attr_default_value';
    const ATTRIBUTE_NAME_SAVE_ATTR_ORDER = 'attr_order';



    // Configuration
    const CONF_ATTRIBUTE_KEY_PATTERN = 'attribute_%1$s';



    // Exception message constants
    const EXCEPT_MSG_ATTRIBUTE_IS_NEW = 'Attribute is new!';
    const EXCEPT_MSG_ATTRIBUTE_KEY_NOT_FOUND = 'Following attribute key "%1$s" not found in attribute collection.';
}