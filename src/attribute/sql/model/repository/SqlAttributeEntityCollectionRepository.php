<?php
/**
 * This class allows to define SQL attribute entity collection repository class.
 * SQL attribute entity collection repository uses SQL table persistence.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\attribute_model\attribute\sql\model\repository;

use liberty_code\attribute_model\attribute\model\repository\AttributeEntityCollectionRepository;

use liberty_code\sql\persistence\table\model\TablePersistor;
use liberty_code\attribute_model\attribute\model\AttributeEntityCollection;
use liberty_code\attribute_model\attribute\model\AttributeEntityFactory;
use liberty_code\attribute_model\attribute\sql\model\repository\SqlAttributeEntityRepository;



/**
 * @method null|SqlAttributeEntityRepository getObjRepository() @inheritdoc
 * @method null|TablePersistor getObjPersistor() @inheritdoc
 */
class SqlAttributeEntityCollectionRepository extends AttributeEntityCollectionRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * DI: Attribute entity collection instance.
     * @var null|AttributeEntityCollection
     */
    protected $objAttributeEntityCollection;



	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param SqlAttributeEntityRepository $objRepository
     * @param AttributeEntityCollection $objAttributeEntityCollection = null
     */
    public function __construct(
        AttributeEntityFactory $objEntityFactory,
        SqlAttributeEntityRepository $objRepository,
        AttributeEntityCollection $objAttributeEntityCollection = null
    )
    {
        // Init properties
        $this->objAttributeEntityCollection = $objAttributeEntityCollection;

        // Call parent constructor
        parent::__construct(
            $objEntityFactory,
            $objRepository
        );
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixRepositoryClassPath()
    {
        // Return result
        return SqlAttributeEntityRepository::class;
    }



}