<?php
/**
 * This class allows to define SQL attribute entity repository class.
 * SQL attribute entity repository uses SQL table persistence.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\attribute_model\attribute\sql\model\repository;

use liberty_code\attribute_model\attribute\model\repository\AttributeEntityRepository;

use liberty_code\sql\persistence\library\ConstPersistor;
use liberty_code\sql\persistence\table\model\TablePersistor;
use liberty_code\attribute_model\attribute\library\ConstAttribute;
use liberty_code\attribute_model\attribute\sql\library\ConstSqlAttribute;



/**
 * @method null|TablePersistor getObjPersistor() @inheritdoc
 */
class SqlAttributeEntityRepository extends AttributeEntityRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param TablePersistor $objPersistor
     */
    public function __construct(TablePersistor $objPersistor)
    {
        // Call parent constructor
        parent::__construct($objPersistor);
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if structure of attribute alias required.
     * Overwrite it to set specific option.
     *
     * @return boolean
     */
    public function checkStructureAliasRequired()
    {
        // Return result
        return true;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixPersistorClassPath()
    {
        // Return result
        return TablePersistor::class;
    }



    /**
     * Get SQL table name where attributes stored.
     * Overwrite it to implement specific table name.
     *
     * @return string
     */
    public function getStrSqlTableName()
    {
        // Return result
        return ConstSqlAttribute::SQL_TABLE_NAME_ATTR;
    }



    /**
     * @inheritdoc
     */
    protected function getTabFixPersistorConfig()
    {
        // Return result
        return array(
            ConstPersistor::TAB_CONFIG_KEY_TABLE_NAME => $this->getStrSqlTableName(),
            ConstPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ID  => ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_ID
        );
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set attribute structure.
     * Return true if correctly run, false else.
     *
     * @return boolean
     */
    public function setStructure()
    {
        // Init var
        $boolStructAliasRequired = $this->checkStructureAliasRequired();
        $objConnection = $this
            ->getObjPersistor()
            ->getObjCommandFactory()
            ->getObjConnection();

        // Create attribute table
        $strSqlTableNm = $this->getStrSqlTableName();
        $strSql = sprintf(
            'CREATE TABLE %1$s (
                %2$s int(10) unsigned NOT NULL AUTO_INCREMENT,
                %3$s datetime NOT NULL,
                %4$s datetime NOT NULL,
                %5$s varchar(100) NOT NULL,
                %6$s varchar(100) NOT NULL,
                %7$s tinyint(1) NOT NULL,
                %8$s text NOT NULL,
                %9$s text NOT NULL,
                %10$s text NOT NULL,
                %11$s int(10) unsigned NOT NULL,
                PRIMARY KEY (%2$s),
                UNIQUE KEY %5$s (%5$s)
            );',
            $objConnection->getStrEscapeName($strSqlTableNm),
            $objConnection->getStrEscapeName(ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_ID),
            $objConnection->getStrEscapeName(ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_DT_CREATE),
            $objConnection->getStrEscapeName(ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_DT_UPDATE),
            $objConnection->getStrEscapeName(ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_NM),
            $objConnection->getStrEscapeName(ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_DATA_TYPE),
            $objConnection->getStrEscapeName(ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_VALUE_REQUIRED),
            $objConnection->getStrEscapeName(ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_LIST_VALUE),
            $objConnection->getStrEscapeName(ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_RULE_CNF),
            $objConnection->getStrEscapeName(ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_DEFAULT_VALUE),
            $objConnection->getStrEscapeName(ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_ORDER)
        );
        $result = ($objConnection->execute($strSql) !== false);

        // Create indexes, if required
        if($result)
        {
            // Create data type index
            $strSql = sprintf(
                'CREATE INDEX %1$s ON %2$s (%1$s);',
                $objConnection->getStrEscapeName(ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_DATA_TYPE),
                $objConnection->getStrEscapeName($strSqlTableNm)
            );
            $result = ($objConnection->execute($strSql) !== false) && $result;
        }

        // Create alias column, if required
        if($result && $boolStructAliasRequired)
        {
            $strSql = sprintf(
                'ALTER TABLE %1$s 
                ADD %2$s varchar(100) NOT NULL 
                AFTER %3$s;',
                $objConnection->getStrEscapeName($strSqlTableNm),
                $objConnection->getStrEscapeName(ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_ALIAS),
                $objConnection->getStrEscapeName(ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_NM)
            );
            $result = ($objConnection->execute($strSql) !== false);

            // Create alias index, if required
            if($result)
            {
                $strSql = sprintf(
                    'CREATE INDEX %1$s ON %2$s (%1$s);',
                    $objConnection->getStrEscapeName(ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_ALIAS),
                    $objConnection->getStrEscapeName($strSqlTableNm)
                );
                $result = ($objConnection->execute($strSql) !== false);
            }
        }

        // Return result
        return $result;
    }



}