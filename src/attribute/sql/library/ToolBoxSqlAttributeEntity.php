<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\attribute_model\attribute\sql\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\attribute_model\attribute\library\ConstAttribute;
use liberty_code\attribute_model\attribute\model\AttributeEntity;
use liberty_code\attribute_model\attribute\sql\model\repository\SqlAttributeEntityRepository;



class ToolBoxSqlAttributeEntity extends Multiton
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * Only 1 instance authorized (Singleton)
     * @var int
     */
    static protected $__instanceIntCountLimit = 1;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get additional rule configurations array,
     * for specified attribute entity.
     * Return format: @see AttributeEntity::getTabRuleConfig() .
     *
     * @param AttributeEntity $objAttributeEntity
     * @param SqlAttributeEntityRepository $objAttributeEntityRepo
     * @return array
     */
    public static function getTabRuleConfig(
        AttributeEntity $objAttributeEntity,
        SqlAttributeEntityRepository $objAttributeEntityRepo
    )
    {
        // Init var
        $boolAttrAliasRequired = $objAttributeEntity->checkAttrAliasRequired();
        $result = array(
            ConstAttribute::ATTRIBUTE_KEY_ID => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => [
                                'is_null',
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() use ($objAttributeEntity)
                                        {return $objAttributeEntity->checkIsNew();}
                                    ]
                                ]
                            ],
                            'is-valid-id' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    [
                                        'compare_value' => 0,
                                        'equal_enable_require' => false
                                    ]
                                ],
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() use ($objAttributeEntity)
                                        {return (!$objAttributeEntity->checkIsNew());}
                                    ]
                                ],
                                [
                                    'sql_exist',
                                    [
                                        'command_factory' => $objAttributeEntityRepo->getObjPersistor()->getObjCommandFactory(),
                                        'table_name' => $objAttributeEntityRepo->getStrSqlTableName(),
                                        'column_name' => $objAttributeEntity->getAttributeNameSave(ConstAttribute::ATTRIBUTE_KEY_ID)
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null if new, or a valid ID else.'
                    ]
                ]
            ],
            ConstAttribute::ATTRIBUTE_KEY_NAME => [
                'type_string',
                [
                    'sub_rule_not',
                    [
                        'rule_config' => ['is_empty'],
                        'error_message_pattern' => '%1$s is empty.'
                    ]
                ],
                [
                    'sub_rule_not',
                    [
                        'rule_config' => [
                            [
                                'sql_exist',
                                [
                                    'command_factory' => $objAttributeEntityRepo->getObjPersistor()->getObjCommandFactory(),
                                    'table_name' => $objAttributeEntityRepo->getStrSqlTableName(),
                                    'column_name' => $objAttributeEntity->getAttributeNameSave(ConstAttribute::ATTRIBUTE_KEY_NAME),
                                    'exclude' => [
                                        $objAttributeEntity->getAttributeNameSave(ConstAttribute::ATTRIBUTE_KEY_ID) =>
                                            $objAttributeEntity->getAttributeValueSave(ConstAttribute::ATTRIBUTE_KEY_ID)
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be unique.'
                    ]
                ]
            ]
        );

        // Set attribute alias rule configuration, if required
        if($boolAttrAliasRequired)
        {
            $result[ConstAttribute::ATTRIBUTE_KEY_ALIAS] = array(
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-string' => [
                                'type_string',
                                [
                                    'sub_rule_not',
                                    [
                                        'rule_config' => ['is_empty'],
                                    ]
                                ],
                                [
                                    'sub_rule_not',
                                    [
                                        'rule_config' => [
                                            [
                                                'sql_exist',
                                                [
                                                    'command_factory' => $objAttributeEntityRepo->getObjPersistor()->getObjCommandFactory(),
                                                    'table_name' => $objAttributeEntityRepo->getStrSqlTableName(),
                                                    'column_name' => $objAttributeEntity->getAttributeNameSave(ConstAttribute::ATTRIBUTE_KEY_ALIAS),
                                                    'exclude' => [
                                                        $objAttributeEntity->getAttributeNameSave(ConstAttribute::ATTRIBUTE_KEY_ID) =>
                                                            $objAttributeEntity->getAttributeValueSave(ConstAttribute::ATTRIBUTE_KEY_ID)
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null or a unique string not empty.'
                    ]
                ]
            );
        }

        // Return result
        return $result;
    }



}