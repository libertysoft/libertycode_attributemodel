<?php
/**
 * GET arguments:
 * - clean_attribute: = 1: Remove attributes.
 */

// Init var
$strTestRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strTestRootAppPath . '/src/attribute/test/AttributeRepositoryTest.php');

// Use
use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\attribute_model\attribute\library\ConstAttribute;
use liberty_code\attribute_model\attribute\repository\model\SaveAttributeEntityCollection;



// Init var
$objAttributeEntityCollection = new SaveAttributeEntityCollection();



// Init builder
$tabDataSrc = array(
    'attr-1' => [
        'strAttrName' => 'Attr 1',
        'strAttrAlias' => 'attr-1',
        'strAttrDataType' => 'string',
        'boolAttrValueRequired' => true,
        'attrDefaultValue' => 'text 1',
        'intAttrOrder' => 4
    ],
    'Attr 2' => [
        'strAttrDataType' => 'integer',
        'boolAttrValueRequired' => true,
        'tabAttrRuleConfig' => [
            [
                'compare_between',
                [
                    'greater_compare_value' => 5,
                    'less_compare_value' => 10,
                ]
            ]
        ],
        'attrDefaultValue' => 5,
        'intAttrOrder' => 3
    ],
    [
        'strAttrName' => 'Attr 3',
        'strAttrAlias' => 'attr-3',
        'strAttrDataType' => 'boolean',
        'attrDefaultValue' => true,
        'intAttrOrder' => 2
    ],
    'Attr 4' => [
        'strAttrDataType' => 'string',
        'tabAttrListValue' => ['', 'test-1', 'test-2', 'test-3']
    ],
);
$objAttributeBuilder->setTabDataSrc($tabDataSrc);



// Init structure
echo('Test set structure: <pre>');
var_dump($objAttributeEntityRepo->setStructure());
echo('</pre>');

echo('<br /><br /><br />');



// Test hydrate attribute entity collection
echo('Test hydrate attribute entity collection: <br />');

echo('Before hydrate: <pre>');
foreach($objAttributeEntityCollection->getTabKey() as $strKey)
{
    var_dump($objAttributeEntityCollection->getItem($strKey)->getTabData());
}
echo('</pre>');

$objAttributeBuilder->hydrateAttributeCollection($objAttributeEntityCollection);

echo('After hydrate: <pre>');
foreach($objAttributeEntityCollection->getTabKey() as $strKey)
{
    var_dump($objAttributeEntityCollection->getItem($strKey)->getTabData());
}
echo('</pre>');

echo('<br /><br /><br />');



// Save attribute entities
echo('Test save attribute entities: ');

$boolValid = true;
$tabError = array();
$boolValid = $objAttributeEntityCollection->checkValid(null, null, $tabError);

if($boolValid)
{
    echo('<pre>');
    var_dump($objAttributeEntityCollectionRepo->save($objAttributeEntityCollection));
    echo('</pre>');
}
else
{
    echo('Validation failed:<pre>');var_dump($tabError);echo('</pre>');
}

echo('After save: <pre>');
$tabId = array();
foreach($objAttributeEntityCollection->getTabKey() as $strKey)
{
    $objAttributeEntity = $objAttributeEntityCollection->getItem($strKey);
    $tabId[] = $objAttributeEntity->getAttributeValue(ConstAttribute::ATTRIBUTE_KEY_ID);
    var_dump($objAttributeEntity->getTabData());
}
echo('</pre>');

echo('<br /><br /><br />');



// Load attribute entities
$objAttributeEntityCollection = new SaveAttributeEntityCollection();

echo('Test load attribute entities: <pre>');
$boolLoad = (
    ($boolValid) ?
        $objAttributeEntityCollectionRepo->load($objAttributeEntityCollection, $tabId) :
        $objAttributeEntityCollectionRepo->search(
            $objAttributeEntityCollection,
            array(
                'select' => [
                    ['pattern' => '*']
                ],
                'from' => [
                    $objAttributeEntityRepo->getStrSqlTableName()
                ],
                'order' => [
                    [
                        'operand' => [
                            'column_name' => ConstAttribute::ATTRIBUTE_NAME_SAVE_ATTR_ID
                        ]
                    ]
                ],
                'limit' => [
                    'count' => 4
                ]
            )
        )
);
var_dump($boolLoad);
echo('</pre>');

echo('After load: <pre>');
foreach($objAttributeEntityCollection->getTabKey() as $strKey)
{
    var_dump($objAttributeEntityCollection->getItem($strKey)->getTabData());
}
echo('</pre>');

echo('<br /><br /><br />');



// Test get attribute entity
echo('Test get attribute entity: <br />');

foreach($objAttributeEntityCollection->getTabAttributeKey() as $strAttributeKey)
{
    echo('Test get attribute key "'.$strAttributeKey.'": <br />');

    $objAttributeEntity = $objAttributeEntityCollection->getObjAttribute($strAttributeKey);

    echo('Get attribute class: <pre>');var_dump(get_class($objAttributeEntity));echo('</pre>');
    echo('Check attribute key exists: <pre>');var_dump($objAttributeEntityCollection->checkAttributeExists($strAttributeKey));echo('</pre>');
    echo('Get attribute key: <pre>');var_dump($objAttributeEntity->getStrAttributeKey());echo('</pre>');
    echo('Get attribute configuration: <pre>');var_dump($objAttributeEntity->getTabAttributeConfig());echo('</pre>');

    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test get entity attribute configuration
echo('Test get entity attribute configuration: <br />');

echo('Get attribute configuration: <pre>');
var_dump($objAttributeEntityCollection->getTabEntityAttrConfig());
echo('</pre>');

echo('Get rule configurations: <pre>');
var_dump($objAttributeEntityCollection->getTabEntityAttrRuleConfig());
echo('</pre>');

$tabAttrValue = array(
    $objAttributeEntityCollection[0]->getStrAttributeKey() => ['test', 7], // Ok
    'test' => [], // Ko: Not found
    $objAttributeEntityCollection[1]->getStrAttributeKey() => [7, 'test'], // Ok
    $objAttributeEntityCollection[2]->getStrAttributeKey() => [false, true, 0, 1, 7, 'test'] // Ok
);
foreach($tabAttrValue as $strAttributeKey => $tabValue)
{
    foreach($tabValue as $value)
    {
        echo('Test value, for following attribute key "'.$strAttributeKey.'":');
        var_dump($value);
        echo('<br />');

        echo('Get format get value: <pre>');
        var_dump($objAttributeEntityCollection->getEntityAttrValueFormatGet($strAttributeKey, $value));
        echo('</pre>');

        echo('Get format set value: <pre>');
        var_dump($objAttributeEntityCollection->getEntityAttrValueFormatSet($strAttributeKey, $value));
        echo('</pre>');

        echo('Get save format get value: <pre>');
        var_dump($objAttributeEntityCollection->getEntityAttrValueSaveFormatGet($strAttributeKey, $value));
        echo('</pre>');

        echo('Get save format set value: <pre>');
        var_dump($objAttributeEntityCollection->getEntityAttrValueSaveFormatSet($strAttributeKey, $value));
        echo('</pre>');

        echo('<br />');
    }
}

echo('<br /><br /><br />');



// Remove attribute entities, if required
if(trim(ToolBoxTable::getItem($_GET, 'clean_attribute', '0')) == '1')
{
    echo('Test remove attribute entities: <pre>');
    var_dump($objAttributeEntityCollectionRepo->remove($objAttributeEntityCollection));
    echo('</pre>');

    echo('After remove: <pre>');
    foreach($objAttributeEntityCollection->getTabKey() as $strKey)
    {
        var_dump($objAttributeEntityCollection->getItem($strKey)->getTabData());
    }
    echo('</pre>');

    echo('<br /><br /><br />');
}



// Test remove all attributes
$objAttributeEntityCollection->removeAttributeAll();

echo('Test remove all attributes: <br />');

echo('Get attribute keys: <pre>');
var_dump($objAttributeEntityCollection->getTabAttributeKey());
echo('</pre>');

echo('Get attribute configuration: <pre>');
var_dump($objAttributeEntityCollection->getTabEntityAttrConfig());
echo('</pre>');

echo('Get rule configurations: <pre>');
var_dump($objAttributeEntityCollection->getTabEntityAttrRuleConfig());
echo('</pre>');

echo('<br /><br /><br />');



// Remove test database, if required
require_once($strTestRootAppPath . '/src/attribute/test/db/HelpRemoveDbTest.php');


