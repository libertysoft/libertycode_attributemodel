<?php

// Init var
$strAttrRepoRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strAttrRepoRootAppPath . '/src/attribute/test/db/HelpCreateDbTest.php');
require_once($strAttrRepoRootAppPath . '/src/attribute/test/AttributeTest.php');

// Use
use liberty_code\di\dependency\preference\model\Preference;

use liberty_code\attribute_model\attribute\repository\model\SaveAttributeEntityCollection;
use liberty_code\attribute_model\attribute\sql\model\repository\SqlAttributeEntityRepository;
use liberty_code\attribute_model\attribute\sql\model\repository\SqlAttributeEntityCollectionRepository;



// Init repository
$objAttributeEntityRepo = new SqlAttributeEntityRepository($objTablePersistor);

$objPref = new Preference(array(
    'source' => SqlAttributeEntityRepository::class,
    'set' =>  ['type' => 'instance', 'value' => $objAttributeEntityRepo],
    'option' => [
        'shared' => true
    ]
));
$objProvider->getObjDependencyCollection()->setDependency($objPref);

// Init collection repository
$objAttributeEntityCollectionRepo = new SqlAttributeEntityCollectionRepository(
    $objAttributeFactory,
    $objAttributeEntityRepo,
    new SaveAttributeEntityCollection()
);


