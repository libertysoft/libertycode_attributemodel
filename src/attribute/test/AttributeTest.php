<?php

// Init var
$strAttributeRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strAttributeRootAppPath . '/src/attribute/test/ValidatorTest.php');
require_once($strAttributeRootAppPath . '/vendor/liberty_code/handle_model/test/attribute/specification/type/build/boot/DataTypeBuilderBootstrap.php');
require_once($strAttributeRootAppPath . '/vendor/liberty_code/handle_model/test/attribute/specification/boot/AttrSpecBootstrap.php');

// Use
use liberty_code\di\dependency\model\DefaultDependencyCollection;
use liberty_code\di\dependency\preference\model\Preference;
use liberty_code\di\provider\model\DefaultProvider;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\datetime\factory\library\ConstDateTimeFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\model\datetime\factory\model\DefaultDateTimeFactory;
use liberty_code\handle_model\attribute\specification\api\AttrSpecInterface;
use liberty_code\handle_model\attribute\build\model\DefaultBuilder;
use liberty_code\attribute_model\attribute\repository\sql\model\SqlSaveAttributeEntityFactory;



// Init DI
$objDepCollection = new DefaultDependencyCollection();
$objProvider = new DefaultProvider($objDepCollection);



// Init data type collection
$tabDataSrc = array(
    [
        'type' => 'string',
        'config' => [
            'type' => 'string'
        ]
    ],
    [
        'type' => 'numeric',
        'config' => [
            'type' => 'numeric'
        ]
    ],
    [
        'type' => 'numeric',
        'config' => [
            'type' => 'integer',
            'integer_require' => true,
            'greater_compare_value' => 0
        ]
    ],
    [
        'type' => 'boolean',
        'config' => [
            'type' => 'boolean'
        ]
    ],
    [
        'type' => 'date',
        'config' => [
            'type' => 'date'
        ]
    ]
);
$objDataTypeBuilder->setTabDataSrc($tabDataSrc);
$objDataTypeBuilder->hydrateDataTypeCollection($objDataTypeCollection, true);



// Init validator
$objPref = new Preference(array(
    'source' => ValidatorInterface::class,
    'set' =>  ['type' => 'instance', 'value' => $objValidator],
    'option' => [
        'shared' => true
    ]
));
$objProvider->getObjDependencyCollection()->setDependency($objPref);



// Init datetime factory
$tabConfig = array(
    // ConstDateTimeFactory::TAB_CONFIG_KEY_ENTITY_TIMEZONE_NAME => 'UTC',
    ConstDateTimeFactory::TAB_CONFIG_KEY_GET_TIMEZONE_NAME => 'Europe/Paris',
    ConstDateTimeFactory::TAB_CONFIG_KEY_GET_DATETIME_FORMAT => 'Y-m-d H:i:s.u',
    ConstDateTimeFactory::TAB_CONFIG_KEY_SET_TIMEZONE_NAME => 'America/New_York',
    ConstDateTimeFactory::TAB_CONFIG_KEY_SET_DATETIME_FORMAT => 'Y-m-d H:i:s.u',
    // ConstDateTimeFactory::TAB_CONFIG_KEY_SAVE_TIMEZONE_NAME => 'UTC',
    ConstDateTimeFactory::TAB_CONFIG_KEY_SAVE_DATETIME_FORMAT => 'Y-m-d H:i:s'
);
$objDateTimeFactory = new DefaultDateTimeFactory($tabConfig);

$objPref = new Preference(array(
    'source' => DateTimeFactoryInterface::class,
    'set' =>  ['type' => 'instance', 'value' => $objDateTimeFactory],
    'option' => [
        'shared' => true
    ]
));
$objProvider->getObjDependencyCollection()->setDependency($objPref);



// Init attribute
$objPref = new Preference(array(
    'source' => AttrSpecInterface::class,
    'set' =>  ['type' => 'instance', 'value' => $objAttrSpec],
    'option' => [
        'shared' => true
    ]
));
$objProvider->getObjDependencyCollection()->setDependency($objPref);

$objAttributeFactory = new SqlSaveAttributeEntityFactory($objProvider);
$objAttributeBuilder = new DefaultBuilder($objAttributeFactory);


