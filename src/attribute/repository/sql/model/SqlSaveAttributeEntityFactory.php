<?php
/**
 * This class allows to define SQL save attribute entity factory class.
 * SQL save attribute entity factory is save attribute entity factory,
 * to provide new SQL save attribute entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\attribute_model\attribute\repository\sql\model;

use liberty_code\attribute_model\attribute\repository\model\SaveAttributeEntityFactory;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\handle_model\attribute\specification\api\AttrSpecInterface;
use liberty_code\handle_model\attribute\api\AttributeInterface;
use liberty_code\attribute_model\attribute\sql\model\repository\SqlAttributeEntityRepository;
use liberty_code\attribute_model\attribute\repository\sql\model\SqlSaveAttributeEntity;



/**
 * @method SqlSaveAttributeEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 * @method null|SqlSaveAttributeEntity getObjAttribute(array $tabConfig = array(), string $strConfigKey = null, AttributeInterface $objAttribute = null) @inheritdoc
 */
class SqlSaveAttributeEntityFactory extends SaveAttributeEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => SqlSaveAttributeEntity::class
        );
    }



    /**
     * @inheritdoc
     * @return SqlSaveAttributeEntity
     */
    protected function getObjEntityNew(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $objDateTimeFactory = $this->getObjInstance(DateTimeFactoryInterface::class);
        $objAttrSpec = $this->getObjInstance(AttrSpecInterface::class);
        $objRepository = $this->getObjInstance(SqlAttributeEntityRepository::class);
        $result = new SqlSaveAttributeEntity(
            array(),
            $objValidator,
            $objDateTimeFactory,
            $objAttrSpec,
            $objRepository
        );

        // Return result
        return $result;
    }



}