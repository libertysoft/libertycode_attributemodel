<?php
/**
 * This class allows to define SQL save attribute entity class.
 * SQL save attribute entity allows to design a save attribute entity class,
 * using SQL rules for attributes validation.
 *
 * SQL save attribute entity uses the following specified configuration:
 * [
 *     Save attribute entity configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\attribute_model\attribute\repository\sql\model;

use liberty_code\attribute_model\attribute\repository\model\SaveAttributeEntity;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\handle_model\attribute\specification\api\AttrSpecInterface;
use liberty_code\attribute_model\attribute\sql\library\ToolBoxSqlAttributeEntity;
use liberty_code\attribute_model\attribute\sql\model\repository\SqlAttributeEntityRepository;



class SqlSaveAttributeEntity extends SaveAttributeEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * SQL attribute entity repository instance.
     * @var null|SqlAttributeEntityRepository
     */
    protected $objRepository;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param SqlAttributeEntityRepository $objRepository = null
     */
    public function __construct(
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        DateTimeFactoryInterface $objDateTimeFactory = null,
        AttrSpecInterface $objAttrSpec = null,
        SqlAttributeEntityRepository $objRepository = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $tabValue,
            $objValidator,
            $objDateTimeFactory,
            $objAttrSpec
        );

        // Init SQL attribute entity repository
        $this->setRepository($objRepository);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init var
        $result = parent::getTabRuleConfig();
        $objValidator = $this->objValidator;
        $objRepository = $this->getObjRepository();

        // Get rule configurations, if required
        if(
            (!is_null($objValidator)) &&
            (!is_null($objRepository))
        )
        {
            $result = array_merge(
                $result,
                ToolBoxSqlAttributeEntity::getTabRuleConfig($this, $objRepository)
            );
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get SQL attribute entity repository object.
     *
     * @return null|SqlAttributeEntityRepository
     */
    public function getObjRepository()
    {
        // Return result
        return $this->objRepository;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set SQL attribute entity repository object.
     *
     * @param SqlAttributeEntityRepository $objRepository = null
     */
    public function setRepository(SqlAttributeEntityRepository $objRepository = null)
    {
        $this->objRepository = $objRepository;
    }



}