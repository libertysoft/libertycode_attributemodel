<?php
/**
 * This class allows to define save attribute entity factory class.
 * Save attribute entity factory is attribute entity factory,
 * to provide new save attribute entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\attribute_model\attribute\repository\model;

use liberty_code\attribute_model\attribute\model\AttributeEntityFactory;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\handle_model\attribute\specification\api\AttrSpecInterface;
use liberty_code\handle_model\attribute\api\AttributeInterface;
use liberty_code\attribute_model\attribute\repository\model\SaveAttributeEntity;



/**
 * @method SaveAttributeEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 * @method null|SaveAttributeEntity getObjAttribute(array $tabConfig = array(), string $strConfigKey = null, AttributeInterface $objAttribute = null) @inheritdoc
 */
class SaveAttributeEntityFactory extends AttributeEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => SaveAttributeEntity::class
        );
    }



    /**
     * @inheritdoc
     * @return SaveAttributeEntity
     */
    protected function getObjEntityNew(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $objDateTimeFactory = $this->getObjInstance(DateTimeFactoryInterface::class);
        $objAttrSpec = $this->getObjInstance(AttrSpecInterface::class);
        $result = new SaveAttributeEntity(
            array(),
            $objValidator,
            $objDateTimeFactory,
            $objAttrSpec
        );

        // Return result
        return $result;
    }



}