<?php
/**
 * This class allows to define save attribute entity class.
 * Save attribute entity allows to design a save attribute class,
 * using entity.
 *
 * Save attribute entity uses the following specified configuration:
 * [
 *     Attribute entity configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\attribute_model\attribute\repository\model;

use liberty_code\attribute_model\attribute\model\AttributeEntity;
use liberty_code\handle_model\attribute\repository\api\SaveAttributeInterface;

use liberty_code\model\entity\repository\library\ConstSaveEntity;



class SaveAttributeEntity extends AttributeEntity implements SaveAttributeInterface
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabEntityAttrConfig()
    {
        // Init var
        $strAttributeKey = $this->getStrAttributeKey();
        $result = array_merge(
            parent::getTabEntityAttrConfig(),
            array(
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => $strAttributeKey,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true
            )
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getEntityAttrValueSaveFormatGet($value)
    {
        // Init var
        $objAttrSpec = $this->getObjAttrSpec();
        $strDataType = $this->getStrAttributeDataType();
        $result = (
            (!is_null($objAttrSpec)) ?
                $objAttrSpec->getDataTypeValueSaveFormatGet($strDataType, $value) :
                $value
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getEntityAttrValueSaveFormatSet($value)
    {
        // Init var
        $objAttrSpec = $this->getObjAttrSpec();
        $strDataType = $this->getStrAttributeDataType();
        $result = (
            (!is_null($objAttrSpec)) ?
                $objAttrSpec->getDataTypeValueSaveFormatSet($strDataType, $value) :
                $value
        );

        // Return result
        return $result;
    }



}