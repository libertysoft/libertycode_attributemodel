<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/attribute/library/ConstAttribute.php');
include($strRootPath . '/src/attribute/exception/AttributeKeyNotFoundException.php');
include($strRootPath . '/src/attribute/model/AttributeEntity.php');
include($strRootPath . '/src/attribute/model/AttributeEntityCollection.php');
include($strRootPath . '/src/attribute/model/AttributeEntityFactory.php');
include($strRootPath . '/src/attribute/model/repository/AttributeEntityRepository.php');
include($strRootPath . '/src/attribute/model/repository/AttributeEntityCollectionRepository.php');

include($strRootPath . '/src/attribute/sql/library/ConstSqlAttribute.php');
include($strRootPath . '/src/attribute/sql/library/ToolBoxSqlAttributeEntity.php');
include($strRootPath . '/src/attribute/sql/model/SqlAttributeEntity.php');
include($strRootPath . '/src/attribute/sql/model/SqlAttributeEntityFactory.php');
include($strRootPath . '/src/attribute/sql/model/repository/SqlAttributeEntityRepository.php');
include($strRootPath . '/src/attribute/sql/model/repository/SqlAttributeEntityCollectionRepository.php');

include($strRootPath . '/src/attribute/repository/model/SaveAttributeEntity.php');
include($strRootPath . '/src/attribute/repository/model/SaveAttributeEntityCollection.php');
include($strRootPath . '/src/attribute/repository/model/SaveAttributeEntityFactory.php');

include($strRootPath . '/src/attribute/repository/sql/model/SqlSaveAttributeEntity.php');
include($strRootPath . '/src/attribute/repository/sql/model/SqlSaveAttributeEntityFactory.php');

include($strRootPath . '/src/attribute/browser/library/ConstAttributeBrowser.php');
include($strRootPath . '/src/attribute/browser/model/AttributeBrowserEntity.php');
include($strRootPath . '/src/attribute/browser/model/repository/AttributeBrowserRepository.php');

include($strRootPath . '/src/attribute/browser/sql/library/ConstSqlAttributeBrowser.php');
include($strRootPath . '/src/attribute/browser/sql/model/repository/SqlAttributeBrowserRepository.php');

include($strRootPath . '/src/provider/library/ConstRepoAttrProvider.php');
include($strRootPath . '/src/provider/exception/AttributeEntityCollectionRepoInvalidFormatException.php');
include($strRootPath . '/src/provider/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/provider/model/RepoAttrProvider.php');

include($strRootPath . '/src/provider/sql/model/SqlRepoAttrProvider.php');